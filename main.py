import tensorflow as tf
import numpy as np
import pathlib
import cv2
import matplotlib.pyplot as plt

# reminder: copy CUDA dll's into anaconda3/envs/<project>/Library/bin

class Match:
    def __init__(self):
        ROOT_PATH = pathlib.Path(__file__).parent.absolute()
        IMG_PATH = ROOT_PATH.joinpath("Megurinemotion ruka_bg.jpg") # (996, 920, 3)
        print(IMG_PATH)

        img = cv2.imread(str(IMG_PATH))
        print(img.shape) # row,col,ch

        crop_center_ij = (200,460)
        crop_half_height = 100
        crop_half_width = 150

        img_cropped = img[crop_center_ij[0]-crop_half_height:crop_center_ij[0]+crop_half_height,crop_center_ij[1]-crop_half_width:crop_center_ij[1]+crop_half_width,:]
        print(img_cropped.shape)

        plt.figure()
        plt.imshow(np.flip(img_cropped,axis=2))
        plt.show()

        self.img = img
        self.img_cropped = img_cropped

        self.model = self.build_model()
        return

    def build_model(self):
        model = tf.keras.applications.mobilenet_v2.MobileNetV2(weights='imagenet', include_top=False)
        for layer in model.layers: # https://stackoverflow.com/questions/50283844/in-keras-how-to-get-the-layer-name-associated-with-a-model-object-contained-i
            print(layer.name, layer.output_shape)

        print('------------------------')
        for layer in model.layers:
            if layer.name[-3:]=='add':
                print(layer.name, layer.output_shape)
        return model


if __name__ == "__main__":
    tf.config.set_visible_devices([], 'GPU') # https://github.com/tensorflow/tensorflow/issues/31135#issuecomment-572213168

    x = Match()

    print('-- done')
    
    # : before using complicated feature extractor, first use identity function
    # : create search strategy function and affine transformation function with alpha mask
    # : alpha mask: initialize as ones(image rows, image cols); when transforming, fill empty area w/ zeeros
    # for search strategy, suppose branching factor = 2
    # then:
    # for x = [-16,16], for y = [-16,16], for theta=[0,pi/2], f(img, affine transform parameters)
    # for x = [-a,a], for y = [-b,b], for theta = [-c,c] -->
    # parameter matrix:
    # [
    # -a, -b, -c
    # -a, -b, +c
    # -a, +b, -c
    # -a, +b, +c
    # +a, -b, -c
    # +a, -b, +c
    # +a, +b, -c
    # +a, +b, +c
    # ]
    # then, argmax and get best row from parameter matrix with parameters [a*, b*, c*]
    # bleh
    #
    #
    # find a h.264 motion estimation library and use it

    # TODO: delete this file
    