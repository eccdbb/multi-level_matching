import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import camera
import pyvista
from scipy.spatial import distance_matrix
#from sklearn.decomposition import PCA, KernelPCA
from octree import Octree
import open3d as o3d
import copy

class Data:
    def __init__(self):
        self.img_pair_list = []
        self.populate_data()
        return

    def populate_data(self):
        # note: left and right are reversed so that flow is positive; TODO: undo reversed filenames and add *-1 @ Z = -1*B*f/flow_x

        # panasonic g85 12-35mm f2.8 @ 12mm f?? // 0..5
        self.img_pair_list.append({'left': 'P1510693.JPG', 'right': 'P1510692.JPG'})  #0 coin table
        self.img_pair_list.append({'left': 'P1510695.JPG', 'right': 'P1510694.JPG'})  #1 room
        self.img_pair_list.append({'left': 'P1510697.JPG', 'right': 'P1510696.JPG'})  #2 curtain, towel, chair
        self.img_pair_list.append({'left': 'P1510699.JPG', 'right': 'P1510698.JPG'})  #3 curtain, socks on pole
        self.img_pair_list.append({'left': 'P1510701.JPG', 'right': 'P1510700.JPG'})  #4 fabric aliasing
        self.img_pair_list.append({'left': 'P1510703.JPG', 'right': 'P1510702.JPG'})  #5 phone glass reflection

        # panasonic g85 25mm f1.7 @ 25mm f??
        self.img_pair_list.append({'left': 'P1510753.JPG', 'right': 'P1510752.JPG'})  #6 floor 6cm
        self.img_pair_list.append({'left': 'P1510755.JPG', 'right': 'P1510754.JPG'})  #7 floor 1cm
        self.img_pair_list.append({'left': 'P1510757.JPG', 'right': 'P1510756.JPG'})  #8 drawer 6cm
        self.img_pair_list.append({'left': 'P1510759.JPG', 'right': 'P1510758.JPG'})  #9 drawer 1cm
        self.img_pair_list.append({'left': 'P1510761.JPG', 'right': 'P1510760.JPG'})  #10 detergent 6cm
        self.img_pair_list.append({'left': 'P1510763.JPG', 'right': 'P1510762.JPG'})  #11 detergent 1cm <=========== uploaded to .git
        self.img_pair_list.append({'left': 'P1510766.JPG', 'right': 'P1510765.JPG'})  #12 chair 6cm
        self.img_pair_list.append({'left': 'P1510768.JPG', 'right': 'P1510767.JPG'})  #13 chair 1cm
        self.img_pair_list.append({'left': 'P1510771.JPG', 'right': 'P1510770.JPG'})  #14 pants 6cm
        self.img_pair_list.append({'left': 'P1510773.JPG', 'right': 'P1510772.JPG'})  #15 pants 1cm
        self.img_pair_list.append({'left': 'P1510775.JPG', 'right': 'P1510774.JPG'})  #16 laptop 6cm
        self.img_pair_list.append({'left': 'P1510777.JPG', 'right': 'P1510776.JPG'})  #17 laptop 1cm
        self.img_pair_list.append({'left': 'P1510779.JPG', 'right': 'P1510778.JPG'})  #18 window 6cm
        self.img_pair_list.append({'left': 'P1510781.JPG', 'right': 'P1510780.JPG'})  #19 window 1cm
        self.img_pair_list.append({'left': 'P1510783.JPG', 'right': 'P1510782.JPG'})  #20 A/C 6cm
        self.img_pair_list.append({'left': 'P1510785.JPG', 'right': 'P1510784.JPG'})  #21 A/C 1cm
        self.img_pair_list.append({'left': 'P1510788.JPG', 'right': 'P1510787.JPG'})  #22 room 6cm
        self.img_pair_list.append({'left': 'P1510790.JPG', 'right': 'P1510789.JPG'})  #23 room 1cm
        self.img_pair_list.append({'left': 'P1510794.JPG', 'right': 'P1510793.JPG'})  #24 chair and floor 6cm
        self.img_pair_list.append({'left': 'P1510796.JPG', 'right': 'P1510795.JPG'})  #25 chair and floor 1cm
        self.img_pair_list.append({'left': 'P1510800.JPG', 'right': 'P1510799.JPG'})  #26 tea 6cm
        self.img_pair_list.append({'left': 'P1510802.JPG', 'right': 'P1510801.JPG'})  #27 tea 1cm // difficult
        self.img_pair_list.append({'left': 'P1510805.JPG', 'right': 'P1510804.JPG'})  #28 desk monitor 6cm
        self.img_pair_list.append({'left': 'P1510807.JPG', 'right': 'P1510806.JPG'})  #29 desk monitor 1cm // difficult
        self.img_pair_list.append({'left': 'P1510810.JPG', 'right': 'P1510809.JPG'})  #30 laptop 6cm
        self.img_pair_list.append({'left': 'P1510812.JPG', 'right': 'P1510811.JPG'})  #31 laptop 1cm // optical flow not good at textureless regions
        self.img_pair_list.append({'left': 'P1510815.JPG', 'right': 'P1510814.JPG'})  #32 shower 6cm
        self.img_pair_list.append({'left': 'P1510817.JPG', 'right': 'P1510816.JPG'})  #33 shower 1cm
        self.img_pair_list.append({'left': 'P1510819.JPG', 'right': 'P1510818.JPG'})  #34 toilet f/2 6cm
        self.img_pair_list.append({'left': 'P1510821.JPG', 'right': 'P1510820.JPG'})  #35 toilet f/2 1cm // difficult; total fail
        self.img_pair_list.append({'left': 'P1510824.JPG', 'right': 'P1510823.JPG'})  #36 toilet f/11 6cm
        self.img_pair_list.append({'left': 'P1510826.JPG', 'right': 'P1510825.JPG'})  #37 toilet f/11 1cm // difficult; total fail
        self.img_pair_list.append({'left': 'P1510828.JPG', 'right': 'P1510827.JPG'})  #38 soap 6cm
        self.img_pair_list.append({'left': 'P1510830.JPG', 'right': 'P1510829.JPG'})  #39 soap 1cm // good; optical flow ray traces through the mirror
        self.img_pair_list.append({'left': 'P1510832.JPG', 'right': 'P1510831.JPG'})  #40 mirror 6cm
        self.img_pair_list.append({'left': 'P1510834.JPG', 'right': 'P1510833.JPG'})  #41 mirror 1cm // optical flow ray traces through the mirror, but is a mess
        self.img_pair_list.append({'left': 'P1510931.JPG', 'right': 'P1510930.JPG'})  #42 window far 6cm
        self.img_pair_list.append({'left': 'P1510933.JPG', 'right': 'P1510932.JPG'})  #43 window far 1cm
        self.img_pair_list.append({'left': 'P1510935.JPG', 'right': 'P1510934.JPG'})  #44 window close 6cm
        self.img_pair_list.append({'left': 'P1510937.JPG', 'right': 'P1510936.JPG'})  #45 window close 1cm // optical flow is confused; rain reflection
        self.img_pair_list.append({'left': 'P1510939.JPG', 'right': 'P1510938.JPG'})  #46 bottle front view 6cm
        self.img_pair_list.append({'left': 'P1510941.JPG', 'right': 'P1510940.JPG'})  #47 bottle front view 1cm // use this point cloud for registration
        self.img_pair_list.append({'left': 'P1510943.JPG', 'right': 'P1510942.JPG'})  #48 bottle side view 6cm
        self.img_pair_list.append({'left': 'P1510945.JPG', 'right': 'P1510944.JPG'})  #49 bottle side view 1cm // use this point cloud for registration
        self.img_pair_list.append({'left': 'P1510947.JPG', 'right': 'P1510946.JPG'})  #50 gorilla A 6cm
        self.img_pair_list.append({'left': 'P1510949.JPG', 'right': 'P1510948.JPG'})  #51 gorilla A 1cm
        self.img_pair_list.append({'left': 'P1510951.JPG', 'right': 'P1510950.JPG'})  #52 gorilla B 6cm
        self.img_pair_list.append({'left': 'P1510953.JPG', 'right': 'P1510952.JPG'})  #53 gorilla B 1cm
        self.img_pair_list.append({'left': 'P1510955.JPG', 'right': 'P1510954.JPG'})  #54 gorilla C 6cm
        self.img_pair_list.append({'left': 'P1510957.JPG', 'right': 'P1510956.JPG'})  #55 gorilla C 1cm
        self.img_pair_list.append({'left': 'P1510960.JPG', 'right': 'P1510959.JPG'})  #56 gorilla D 6cm
        self.img_pair_list.append({'left': 'P1510962.JPG', 'right': 'P1510961.JPG'})  #57 gorilla D 1cm
        self.img_pair_list.append({'left': 'P1510964.JPG', 'right': 'P1510963.JPG'})  #58 desk A 6cm
        self.img_pair_list.append({'left': 'P1510966.JPG', 'right': 'P1510965.JPG'})  #59 desk A 1cm
        self.img_pair_list.append({'left': 'P1510970.JPG', 'right': 'P1510969.JPG'})  #60 desk B 6cm
        self.img_pair_list.append({'left': 'P1510972.JPG', 'right': 'P1510971.JPG'})  #61 desk B 1cm
        self.img_pair_list.append({'left': 'P1510974.JPG', 'right': 'P1510973.JPG'})  #62 desk C 6cm
        self.img_pair_list.append({'left': 'P1510976.JPG', 'right': 'P1510975.JPG'})  #63 desk C 1cm
        self.img_pair_list.append({'left': 'P1510990.JPG', 'right': 'P1510989.JPG'})  #64 coin table A 1cm
        self.img_pair_list.append({'left': 'P1510992.JPG', 'right': 'P1510991.JPG'})  #65 coin table B 1cm
        self.img_pair_list.append({'left': 'P1510994.JPG', 'right': 'P1510993.JPG'})  #66 coin table C 1cm // fail
        self.img_pair_list.append({'left': 'P1510996.JPG', 'right': 'P1510995.JPG'})  #67 coin table D 1cm
        self.img_pair_list.append({'left': 'P1510998.JPG', 'right': 'P1510997.JPG'})  #68 coin table E 1cm
        self.img_pair_list.append({'left': 'P1520001.JPG', 'right': 'P1510999.JPG'})  #69 coin table F 1cm // buggy
        self.img_pair_list.append({'left': 'P1520003.JPG', 'right': 'P1520002.JPG'})  #70 coin table G 1cm // fail
        self.img_pair_list.append({'left': 'P1520015.JPG', 'right': 'P1520014.JPG'})  #71 trees A 6cm
        self.img_pair_list.append({'left': 'P1520017.JPG', 'right': 'P1520016.JPG'})  #72 trees A 1cm // fail
        self.img_pair_list.append({'left': 'P1520023.JPG', 'right': 'P1520022.JPG'})  #73 trees B 6cm
        self.img_pair_list.append({'left': 'P1520025.JPG', 'right': 'P1520024.JPG'})  #74 trees B 1cm // fail

        #panasonic g85 35-100mm f4-5.6 @ 100mm f??
        self.img_pair_list.append({'left': 'P1520033.JPG', 'right': 'P1520032.JPG'})  #75 books A 6cm // fail
        self.img_pair_list.append({'left': 'P1520035.JPG', 'right': 'P1520034.JPG'})  #76 books A 1cm
        self.img_pair_list.append({'left': 'P1520037.JPG', 'right': 'P1520036.JPG'})  #77 books B 6cm // fail
        self.img_pair_list.append({'left': 'P1520039.JPG', 'right': 'P1520038.JPG'})  #78 books B 1cm
        self.img_pair_list.append({'left': 'P1520041.JPG', 'right': 'P1520040.JPG'})  #79 books C 6cm // fail
        self.img_pair_list.append({'left': 'P1520043.JPG', 'right': 'P1520042.JPG'})  #80 books C 1cm
        self.img_pair_list.append({'left': 'P1520045.JPG', 'right': 'P1520044.JPG'})  #81 books D 6cm // fail
        self.img_pair_list.append({'left': 'P1520047.JPG', 'right': 'P1520046.JPG'})  #82 books D 1cm
        self.img_pair_list.append({'left': 'P1520049.JPG', 'right': 'P1520048.JPG'})  #83 books E 6cm // fail
        self.img_pair_list.append({'left': 'P1520051.JPG', 'right': 'P1520050.JPG'})  #84 books E 1cm
        self.img_pair_list.append({'left': 'P1520053.JPG', 'right': 'P1520052.JPG'})  #85 books F 6cm // fail
        self.img_pair_list.append({'left': 'P1520055.JPG', 'right': 'P1520054.JPG'})  #86 books F 1cm

        # panasonic g85 25mm f1.7 @ 25mm f??
        self.img_pair_list.append({'left': 'P1520063.JPG', 'right': 'P1520062.JPG'})  #87 batteries A 6cm // use batteries A-F 1cm for scene flow
        self.img_pair_list.append({'left': 'P1520065.JPG', 'right': 'P1520064.JPG'})  #88 batteries A 1cm
        self.img_pair_list.append({'left': 'P1520067.JPG', 'right': 'P1520066.JPG'})  #89 batteries B 6cm
        self.img_pair_list.append({'left': 'P1520069.JPG', 'right': 'P1520068.JPG'})  #90 batteries B 1cm
        self.img_pair_list.append({'left': 'P1520071.JPG', 'right': 'P1520070.JPG'})  #91 batteries C 6cm
        self.img_pair_list.append({'left': 'P1520073.JPG', 'right': 'P1520072.JPG'})  #92 batteries C 1cm
        self.img_pair_list.append({'left': 'P1520075.JPG', 'right': 'P1520074.JPG'})  #93 batteries D 6cm
        self.img_pair_list.append({'left': 'P1520077.JPG', 'right': 'P1520076.JPG'})  #94 batteries D 1cm
        self.img_pair_list.append({'left': 'P1520079.JPG', 'right': 'P1520078.JPG'})  #95 batteries E 6cm
        self.img_pair_list.append({'left': 'P1520081.JPG', 'right': 'P1520080.JPG'})  #96 batteries E 1cm
        self.img_pair_list.append({'left': 'P1520083.JPG', 'right': 'P1520082.JPG'})  #97 batteries F 6cm
        self.img_pair_list.append({'left': 'P1520085.JPG', 'right': 'P1520084.JPG'})  #98 batteries F 1cm
        self.img_pair_list.append({'left': 'P1520092 converted raw.JPG', 'right': 'P1520091 converted raw.JPG'})  #99 keyboard batteries 1cm
        self.img_pair_list.append({'left': 'P1520094 converted raw.JPG', 'right': 'P1520093 converted raw.JPG'})  #100 keyboard batteries 1cm
        self.img_pair_list.append({'left': 'P1520096 converted raw.JPG', 'right': 'P1520095 converted raw.JPG'})  #101 keyboard batteries 1cm
        self.img_pair_list.append({'left': 'P1520098 converted raw.JPG', 'right': 'P1520097 converted raw.JPG'})  #102 keyboard batteries 1cm
        self.img_pair_list.append({'left': 'P1520100 converted raw.JPG', 'right': 'P1520099 converted raw.JPG'})  #103 keyboard batteries 1cm
        #self.img_pair_list.append({'left': '.JPG', 'right': '.JPG'})  # 6cm
        #self.img_pair_list.append({'left': '.JPG', 'right': '.JPG'})  # 1cm
        #self.img_pair_list.append({'left': '.JPG', 'right': '.JPG'})  # 6cm
        #self.img_pair_list.append({'left': '.JPG', 'right': '.JPG'})  # 1cm

        # synthetic // -3..-1
        self.img_pair_list.append({'left': 'test_1.jpg', 'right': 'test_2.jpg'}) # b&w solid blobs # optical flow not good at this big solid patches
        self.img_pair_list.append({'left': 'test_3.jpg', 'right': 'test_4.jpg'}) # clouds # optical flow not good at repetitive patterns
        self.img_pair_list.append({'left': 'test_5.jpg', 'right': 'test_6.jpg'}) # luka
        return

    def get_data(self, index):

        return self.img_pair_list[index]['left'], self.img_pair_list[index]['right']
# END Data


class Flow:
    def __init__(self, DEBUG_FLAG = True):
        self.DEBUG_FLAG = DEBUG_FLAG
        self.img_L = np.array([])
        self.img_R = np.array([])
        self.img_RGB_L = np.zeros((0,0,3))
        self.img_RGB_R = np.zeros((0,0,3))
        self.img_overlay = np.array([])
        self.flow_forwards = np.array([])
        self.img_flow_forwards = np.array([])
        self.flow_backwards = np.array([])
        self.img_flow_backwards = np.array([])
        self.error = 0
        self.img_error = 0
        self.resize_factor = 1.0

        self.XYZ = None # (3,r*c) [px]
        self.point_cloud_metric = None # (r*c,3) [meters]
        self.Z = None # (r,c) depth map [px]
        self.camera = None
        self.decimation_factor = 50 # for point cloud visualization
        self.depth_thresh = 100 # [meters]; get points closer than this; assume camera at origin

        self.data = Data()
        self.pyvista_figure = pyvista.Plotter()

        return

    def compute_flow(self, data_index = 11, resize_factor = 0.2):
        self.resize_factor = resize_factor

        #img_L_name = self.data.img_pair_list[data_index]['left']
        #img_R_name = self.data.img_pair_list[data_index]['right']
        img_L_name, img_R_name = self.data.get_data(data_index)
        img_L = cv.imread(img_L_name) # order: BGR
        img_R = cv.imread(img_R_name)
        self.img_RGB_L = img_L[::-1] # order: RGB
        self.img_RGB_R = img_R[::-1]
        img_L = cv.cvtColor(img_L, cv.COLOR_BGR2GRAY)
        img_R = cv.cvtColor(img_R, cv.COLOR_BGR2GRAY)
        dim_L = tuple(reversed([int(x * resize_factor) for x in img_L.shape]))
        dim_R = tuple(reversed([int(x * resize_factor) for x in img_R.shape]))
        img_L = cv.resize(img_L, dim_L, interpolation=cv.INTER_LANCZOS4)
        img_R = cv.resize(img_R, dim_R, interpolation=cv.INTER_LANCZOS4)
        if self.DEBUG_FLAG: print(img_L.shape)

        # assume images have same dimensions
        dims = list(img_L.shape)
        dims.append(3)

        LR = np.zeros(dims, dtype=np.uint8)

        # reminder: BGR
        LR[:, :, -1] = img_L  # RED
        LR[:, :, -3] = img_R  # BLUE

        def calc(img_A, img_B):
            '''compute flow from A to B'''

            flow_LR_2 = cv.optflow.createOptFlow_DualTVL1()
            flow_LR_2.create()
            flow_LR_2.setScaleStep(0.5)  # (689,918,3) ==> [x*0.5**7 for x in dim_L] ==> [7.171875, 5.3828125]
            flow_LR_2.setScalesNumber(8)
            flow_LR2_out = flow_LR_2.calc(img_A, img_B, None)

            img_flow_LR2 = np.zeros(dims)
            img_flow_LR2[:, :, -1] = flow_LR2_out[:, :, 0] #cv.normalize(flow_LR2_out[:, :, 0], 0, 255, cv.NORM_MINMAX)  # RED
            img_flow_LR2[:, :, -3] = flow_LR2_out[:, :, 1]#cv.normalize(flow_LR2_out[:, :, 1], 0, 255, cv.NORM_MINMAX)  # BLUE
            #img_flow_LR2 = cv.normalize(img_flow_LR2, None, 0, 1.0, cv.NORM_MINMAX)
            flow_max = np.abs(img_flow_LR2).max()
            img_flow_LR2 = img_flow_LR2/flow_max / 2 + 0.5 # -> -1.0..+1.0 -> -0.5..+0.5 -> 0.0..+1.0

            if self.DEBUG_FLAG: print(flow_LR2_out.shape)

            return flow_LR2_out, img_flow_LR2

        flow_LtoR, img_flow_LtoR = calc(img_L, img_R)
        flow_RtoL, img_flow_RtoL = calc(img_R, img_L)

        # compute error image
        error = flow_LtoR+flow_RtoL # TODO: wrong equation, but use for now
        if self.DEBUG_FLAG:
            print('flow_LtoR+flow_RtoL min|max')
            print(error.min(), error.max())

        img_error = np.zeros(dims)
        img_error[:, :, -1] = error[:, :, 0] #cv.normalize(error[:, :, 0], 0, 255, cv.NORM_MINMAX) # RED
        img_error[:, :, -3] = error[:, :, 1] #cv.normalize(error[:, :, 1], 0, 255, cv.NORM_MINMAX) # BLUE
        #img_error = cv.normalize(img_error, None, 0, 1.0, cv.NORM_MINMAX)
        img_error_max = np.abs(img_error).max()
        img_error = img_error/img_error_max / 2 + 0.5

        if self.DEBUG_FLAG:
            print('min|max')
            print(img_flow_LtoR.min(), img_flow_LtoR.max())
            print(img_flow_RtoL.min(), img_flow_RtoL.max())
            print(img_error.min(), img_error.max())

        self.img_L = img_L
        self.img_R = img_R
        self.img_overlay = LR
        self.flow_forwards = flow_LtoR
        self.img_flow_forwards = img_flow_LtoR
        self.flow_backwards = flow_RtoL
        self.img_flow_backwards = img_flow_RtoL
        self.error = error
        self.img_error = img_error

        # set a color patch for visual reference
        if True:
            self.img_flow_forwards[0:100, 0:100, -1] = 0.5
            self.img_flow_forwards[0:100, 0:100, -3] = 0.5
            self.img_flow_backwards[0:100, 0:100, -1] = 0.5
            self.img_flow_backwards[0:100, 0:100, -3] = 0.5
            self.img_error[0:100, 0:100, -1] = 0.5
            self.img_error[0:100, 0:100, -3] = 0.5
        return

    def display(self):
        cv.imshow('img_A img_B', np.concatenate((self.img_L,self.img_R), axis=1))
        cv.imshow('overlay', self.img_overlay)
        #cv.imshow('flow: forwards', self.img_flow_forwards)
        #cv.imshow('flow: backwards', self.img_flow_backwards)
        #cv.imshow('abs error', self.img_error)
        cv.imshow('flow: forwards/backwards/error', np.concatenate((self.img_flow_forwards, self.img_flow_backwards, self.img_error), axis=1))
        #cv.imshow('flowed: A', self.img_L_flowed)
        top = np.concatenate((self.img_L,self.img_R), axis=1)
        bot = np.concatenate((self.img_L_flowed,self.img_R_flowed), axis=1)
        #bot2 = np.concatenate((self.img_L_flowed_inpainted,self.img_R_flowed_inpainted), axis=1)
        cv.imshow('A B flowedA flowedB', np.concatenate((top,bot), axis=0))
        cv.waitKey(1)

        #_ = plt.hist(self.error[:,:,0].flatten(), bins='auto')
        #plt.title('x flow error')
        #plt.show()
        return

    @staticmethod
    def apply_flow(img, flow):
        '''
        apply flow to a grayscale image

        return:
        flowed image
        mask (255 where data exists, else 0)
        image placeholder for inpainting
        '''
        r, c = img.shape

        # original pixel locations
        i, j = np.meshgrid(np.arange(r), np.arange(c))
        ii = i.flatten()
        jj = j.flatten()


        # round flow to nearest neighbor
        flow_int = flow.round().astype(int)
        flow_int_i = flow_int[:,:,1].flatten() # DONE: check opencv flow: x,y or i,j; for now, assume x,y
        flow_int_j = flow_int[:,:,0].flatten()

        # add flow
        new_i = ii + flow_int_i
        new_j = jj + flow_int_j

        # mask: True if within image frame; else False
        mask_i = (new_i >= 0) * (new_i < r) * (new_j >= 0) * (new_j < c)
        mask_j = (new_j >= 0) * (new_j < c) * (new_i >= 0) * (new_i < r)
        mask_i_invert = ~mask_i
        mask_j_invert = ~mask_j


        # apply mask: get True values only
        masked_i = new_i[mask_i]
        masked_j = new_j[mask_j]
        masked_ii = ii[mask_i]
        masked_jj = jj[mask_j]
        masked_i_false = ii[mask_i_invert]
        masked_j_false = jj[mask_j_invert]

        # convert to list # no need
        #masked_i = list(masked_i)
        #masked_j = list(masked_j)

        # use displaced indices to get pixels from img
        new_img = np.zeros_like(img)
        new_img[masked_i, masked_j] = img[masked_ii, masked_jj] # TODO: care: multiple source pixels could land in destination pixel; add buffer and use average
        new_img_mask = np.zeros_like(img)
        new_img_mask[masked_i, masked_j] = 255
        new_img_filled = np.zeros_like(img)
        ref_img_mask = np.zeros_like(img)
        ref_img_mask[masked_ii, masked_jj] = 255


        # DEBUG
        #false_i = new_i[mask_i_invert]
        #false_j = new_j[mask_j_invert]
        #false_ij = np.vstack((false_i,false_j)).T
        #print(false_ij)

        return new_img, new_img_mask, new_img_filled, ref_img_mask

    def apply_flow_to_img(self):
        self.img_L_flowed, self.img_L_flowed_mask, self.img_L_flowed_inpainted, self.img_L_mask = self.apply_flow(self.img_L, self.flow_forwards)
        self.img_R_flowed, self.img_R_flowed_mask, self.img_R_flowed_inpainted, self.img_R_mask = self.apply_flow(self.img_R, self.flow_backwards)
        #cv.xphoto.inpaint(self.img_L_flowed, self.img_L_flowed_mask, self.img_L_flowed_inpainted, cv.xphoto.INPAINT_FSR_FAST) # this doesn't work
        #cv.xphoto.inpaint(self.img_R_flowed, self.img_R_flowed_mask, self.img_R_flowed_inpainted, cv.xphoto.INPAINT_FSR_FAST) # this works
        mask_L = cv.bitwise_not(self.img_L_flowed_mask)
        mask_R = cv.bitwise_not(self.img_R_flowed_mask)
        #self.img_L_flowed_inpainted = cv.inpaint(self.img_L_flowed, mask_L, 3, cv.INPAINT_NS) # works, but slow
        #self.img_R_flowed_inpainted = cv.inpaint(self.img_R_flowed, mask_R, 3, cv.INPAINT_NS)
        return

    def calc_interpolation_error(self):
        '''calculate interpolation error with mask'''
        r,c = self.img_L.shape
        #N = r*c
        N = np.sum(self.img_L_mask>0)
        temp_L = (self.img_L_flowed*self.img_L_flowed_mask - self.img_R*self.img_L_mask)**2
        temp_R = (self.img_R_flowed*self.img_R_flowed_mask - self.img_L*self.img_R_mask)**2
        temp_L = np.sum(temp_L.flatten())
        temp_R = np.sum(temp_R.flatten())
        self.IE_LtoR = (temp_L / N) ** (1/2) # eq. 24 @ "A Database and Evaluation Methodology for Optical Flow" (MSR-TR-2009-179)
        self.IE_RtoL = (temp_R / N) ** (1/2)
        if self.DEBUG_FLAG:
            print('----------------------------')
            print('interpolation error')
            print('% coverage:', N/(r*c))
            print('L -> R:', self.IE_LtoR)
            print('R -> L:', self.IE_RtoL)
        return

    def calc_depth(self,camera_instance):
        # get Camera class and make instance here
        # get baseline B and focal length f in px
        # use cheap way to estimate depth Z: disparity = B*f/Z -> Z = B*f/disparity
        # use flow in x axis for disparity
        p = camera.GetCameraParameters('g85 25mm')
        c = camera.Camera(p.get())
        B = 10 / c.pixel_pitch_x * 1000 # [mm]->[px] // 15926.0 px

        print('assuming rectified stereo setup, small angle approximation')
        print('baseline (mm):', B * c.pixel_pitch_x / 1000)

        flow_x = self.flow_forwards[:,:,0]
        Z = B*c.f / flow_x

        self.camera = c
        self.Z = Z # depth for each pixel
        return

    def backproject(self):
        '''
        first run self.calc_depth()
        then, run self.project() to get point cloud self.XYZ
        self.XYZ.shape = (3,N)
        '''
        # modify camera parameters to support downsampling the image
        self.camera.pp_x = self.camera.pp_x * self.resize_factor
        self.camera.pp_y = self.camera.pp_y * self.resize_factor
        self.camera.f = self.camera.f * self.resize_factor
        self.camera.update() # update for K_inv

        r, c = self.img_L.shape # resized image
        self.camera.N_x = c
        self.camera.N_y = r

        i, j = np.meshgrid(np.arange(r), np.arange(c))
        xy = np.stack((j.flatten(), i.flatten()), axis=1).T # (2,r*c)
        theta = self.camera.theta
        delta = self.camera.delta
        K_inv = self.camera.K_inv
        undistortion_coef = self.camera.undistortion_coef
        depth = np.expand_dims(self.Z.T.flatten(), axis=0) # (1,r*c) # TODO: why need to transpose here?  this works though; maybe figure out why...
        depth = depth * (depth>0) # set negative depth points to zero
        #depth = np.ones_like(depth)*1e8
        XYZ = self.camera.image_to_world_ray_with_distortion(xy,K_inv,theta,delta,undistortion_coef, depth) # (3,r*c) point cloud

        self.XYZ = XYZ
        self.point_cloud_metric = XYZ.T * self.camera.pixel_pitch_x / 1e6 # px * um/px * 1m/1e6um
        return

    def display_point_cloud(self):
        '''display point cloud self.XYZ'''

        print('printing filtered and decimated point cloud')

        p = pyvista.Plotter()

        delta = self.camera.delta
        theta = self.camera.theta
        K_inv = self.camera.K_inv
        undistortion_coef = self.camera.undistortion_coef
        depth = np.linalg.norm([self.camera.N_x, self.camera.N_y, self.camera.f])

        xy_all = np.array([
            [1, 1],
            [1, self.camera.N_y],
            [self.camera.N_x, 1],
            [self.camera.N_x, self.camera.N_y]
        ]).T

        sensor_corners = np.zeros((3, 4))
        for i in range(4):
            xy = xy_all[:, i, None]
            # print(xy.shape) # comment me out; DEBUG
            XYZ = self.camera.image_to_world_ray_with_distortion(xy, K_inv, theta, delta, undistortion_coef, depth)  # sensor corner point
            sensor_corners[:, i] = XYZ.flatten()
            p.add_mesh(pyvista.Line(delta.flatten(), XYZ.flatten()), show_scalar_bar=False, cmap=['red'])  # plot line segment from camera origin to sensor corner

        # draw camera origin as red sphere
        p.add_mesh(pyvista.Sphere(radius=1000, center=delta.flatten()), color='red')

        # draw camera sensor using pyvista PolyData
        sensor_vertices = sensor_corners.T
        sensor_face = np.array([[4, 0, 1, 3, 2]])  # see xy_all for order of points
        sensor_surf = pyvista.PolyData(sensor_vertices, sensor_face)
        p.add_mesh(sensor_surf, show_edges=True)

        # draw point cloud
        points = self.XYZ.T # (N,3)
        thresh = self.depth_thresh / self.camera.pixel_pitch_x * 1e6 # 100 / self.camera.pixel_pitch_x * 1e6 # [m]->[px]
        points = points[np.linalg.norm(points,axis=1)<thresh] # assume camera at origin; get points closer than thresh
        decimation_factor = self.decimation_factor
        pc = pyvista.PolyData(points[::decimation_factor])
        p.add_mesh(pc, color='white')

        # draw line from camera to depth
        #for i in range(0, points.shape[0], 10000):
        #    pt = points[i,:]
        #    p.add_mesh(pyvista.Line(delta.flatten(), pt.flatten()), show_scalar_bar=False, cmap=['white'])

        # draw XYZ unit axes for reference
        unit_axes_scale = 1e6
        unit_X = pyvista.Line(np.array([0, 0, 0]), np.array([1, 0, 0]) * unit_axes_scale)
        unit_Y = pyvista.Line(np.array([0, 0, 0]), np.array([0, 1, 0]) * unit_axes_scale)
        unit_Z = pyvista.Line(np.array([0, 0, 0]), np.array([0, 0, 1]) * unit_axes_scale)
        p.add_mesh(unit_X, show_scalar_bar=False, cmap=['red'])
        p.add_mesh(unit_Y, show_scalar_bar=False, cmap=['green'])
        p.add_mesh(unit_Z, show_scalar_bar=False, cmap=['blue'])

        self.pyvista_figure = p
        self.pyvista_figure.show()
        return

    def save(self):
        np.savez('point_cloud_metric.npz', point_cloud_metric=self.point_cloud_metric)
        return

    def load(self):
        data = np.load('point_cloud_metric.npz')
        self.point_cloud_metric = data['point_cloud_metric']
        data.close()
        return
# END Flow

class SceneFlow:
    '''
    compute scene flow (flow of point clouds (in time)) using
    optical flow from stereo images at time k and k+1
    compute flow from img_L[k]-->img_L[k+1] and calculate velocity vectors
    of point cloud w.r.t. img_L[k]

    ex.
    data_index_dict = {'current': 0, 'next': 1}
    x = SceneFlow(data_index=data_index_dict, display=True)
    print(x.velocity.shape)
    '''

    def __init__(self, data_index, DEBUG_FLAG=False, resize_factor=0.2, display=True):
        '''
        data_index: ex. {'current': 0, 'next': 1} // this will get the 0'th and 1st image pair
        see Data class
        '''
        self.DEBUG_FLAG = DEBUG_FLAG
        self.DISPLAY_OOB_MASK = False

        self.flow_LR = Flow()
        self.flow_LR_next = Flow()
        self.oob_mask = None # out of bounds mask; for visual debug
        self.velocity = np.array([])
        self.velocity_img = np.array([])
        self.oob_mask = np.array([])
        self.img_flow = np.array([])

        self.Ts = 1.0 # sample time [seconds]

        # main
        index_LR = data_index['current']
        index_LR_next = data_index['next']
        self.get_LR(data_index=index_LR, resize_factor=resize_factor)
        self.get_LR_next(data_index=index_LR_next, resize_factor=resize_factor)
        #self.compute_flow_in_time()
        self.compute_flow_in_time_sparse()
        if display: self.display()
        return

    @staticmethod
    def get_flow(data_index:int, resize_factor):
        p = camera.GetCameraParameters('g85 25mm')
        p.info()
        c = camera.Camera(p.get())
        flow = Flow()
        flow.compute_flow(data_index=data_index, resize_factor=resize_factor)
        flow.calc_depth(c)
        flow.backproject()
        return flow

    def get_LR(self, data_index:int, resize_factor):
        # t = k
        self.flow_LR = self.get_flow(data_index, resize_factor)
        return

    def get_LR_next(self, data_index:int, resize_factor):
        # t = k+1
        self.flow_LR_next = self.get_flow(data_index, resize_factor)
        return

    def compute_flow_in_time(self):
        '''
        compute optical flow of L[k]-->L[k+1]
        where k is discrete time index
        L is left camera image
        '''
        if self.DEBUG_FLAG: print('computing dense flow in time...')
        self.DISPLAY_OOB_MASK = True

        img_L_k = self.flow_LR.img_L
        img_L_k_plus = self.flow_LR_next.img_L
        dims = img_L_k.shape
        r = dims[0]
        c = dims[1]

        def calc(img_A, img_B):
            '''compute flow from A to B'''

            flow_LR_2 = cv.optflow.createOptFlow_DualTVL1()
            flow_LR_2.create()
            flow_LR_2.setScaleStep(0.5)  # (689,918,3) ==> [x*0.5**7 for x in dim_L] ==> [7.171875, 5.3828125]
            flow_LR_2.setScalesNumber(8)
            flow_LR2_out = flow_LR_2.calc(img_A, img_B, None)

            img_flow_LR2 = np.zeros((r,c,3))
            img_flow_LR2[:, :, -1] = flow_LR2_out[:, :, 0] #cv.normalize(flow_LR2_out[:, :, 0], 0, 255, cv.NORM_MINMAX)  # RED
            img_flow_LR2[:, :, -3] = flow_LR2_out[:, :, 1]#cv.normalize(flow_LR2_out[:, :, 1], 0, 255, cv.NORM_MINMAX)  # BLUE
            #img_flow_LR2 = cv.normalize(img_flow_LR2, None, 0, 1.0, cv.NORM_MINMAX)
            flow_max = np.abs(img_flow_LR2).max()
            img_flow_LR2 = img_flow_LR2/flow_max / 2 + 0.5 # -> -1.0..+1.0 -> -0.5..+0.5 -> 0.0..+1.0

            if self.DEBUG_FLAG: print(flow_LR2_out.shape)

            return flow_LR2_out, img_flow_LR2

        flow_LtoLnext, img_flowLtoLnext = calc(img_L_k, img_L_k_plus)

        # apply flow to see where the destination pixel coordinates are

        # then, get point cloud at time k: PC
        # and point cloud at time k+1: PC_next
        # to compute point cloud velocity vectors
        def get_idx(i,j):
            '''given pixel coordinates (i,j), get index for point_cloud_metric'''
            idx = c*i + j
            #idx = i + r*j
            return idx # linear index

        i, j = np.meshgrid(np.arange(r), np.arange(c))
        ii = i.flatten()
        jj = j.flatten()
        #if self.DEBUG_FLAG:
        #    print('-----------scene flow indices-----------')
        #    for a,b in zip(ii,jj):
        #        print(a,b)

        # if flow out of bounds, clip to boundary; also, return a binary mask; True if went out of bounds; view as overlay image
        ii2, jj2, oob_mask = self.get_flow_dst(ii,jj,flow_LtoLnext)
        idx_1 = get_idx(ii,jj)
        idx_2 = get_idx(ii2,jj2)

        print('reminder: pixels that flow outside the image boundary are clipped to the boundary')
        print('velocities for these pixels will be incorrect')
        velocity = -1.0*(self.flow_LR_next.point_cloud_metric[idx_2,:] - self.flow_LR.point_cloud_metric[idx_1,:])/self.Ts # Vx,Vy,Vz; (N,3)
        if self.DEBUG_FLAG: print('velocity shape:',velocity.shape) # DEBUG

        pc_next = np.stack((
            self.flow_LR_next.point_cloud_metric[:, 0].reshape(c, r),
            self.flow_LR_next.point_cloud_metric[:, 1].reshape(c, r),
            self.flow_LR_next.point_cloud_metric[:, 2].reshape(c, r)
        ),2)[jj2,ii2,:]
        pc = np.stack((
            self.flow_LR.point_cloud_metric[:, 0].reshape(c, r),
            self.flow_LR.point_cloud_metric[:, 1].reshape(c, r),
            self.flow_LR.point_cloud_metric[:, 2].reshape(c, r)
        ),2)[jj,ii,:]
        velocity = (pc_next-pc)/self.Ts
        velocity = velocity.reshape((c,r,3))
        velocity = np.transpose(velocity, (1,0,2))
        if self.DEBUG_FLAG: print('velocity shape:',velocity.shape)

        #cv.imshow('',
        #          np.hstack((
        #            cv.normalize(self.flow_LR.point_cloud_metric[:,2].reshape(c,r).T, None, 0, 1, cv.NORM_MINMAX),
        #            cv.normalize(self.flow_LR_next.point_cloud_metric[:, 2].reshape(c, r).T, None, 0, 1, cv.NORM_MINMAX),
        #            cv.normalize(self.flow_LR_next.point_cloud_metric[:, 2].reshape(c, r).T-self.flow_LR.point_cloud_metric[:, 2].reshape(c, r).T, None, 0, 1, cv.NORM_MINMAX)
        #          )))
        #cv.imshow('',cv.normalize(pc_next.reshape(c,r,3)[:,:,2].T,None,0,1.0,cv.NORM_MINMAX))

        # convert velocity to a image; cv2 normalize and display it as (Vx,Vy,Vz) -> (R,G,B)
        #velocity_2 = np.array(velocity).reshape((r,c,3)) # DONE: what's the right way to index?
        velocity_2 = velocity

        if self.DEBUG_FLAG: print(velocity_2.shape)
        velocity_2 = np.flip(velocity_2,2) # RGB -> BGR for opencv
        velocity_normalized = cv.normalize(velocity_2, None, 0, 1.0, cv.NORM_MINMAX)


        self.velocity = velocity_2
        self.velocity_img = velocity_normalized
        self.oob_mask = oob_mask
        self.img_flow = img_flowLtoLnext
        #self.display() # DEBUG
        self.pc = pc
        self.pc_next = pc_next
        return

    @staticmethod
    def get_flow_dst(i, j, flow):
        '''
        (i,j): original indices; each (N,1) vectors
        flow: (rows,columns,2) optical flow map

        return destination indices (i_dst,j_dst) by applying flow
        rounded to nearest integer and clipped to image boundary

        also, return out of bounds mask; True if out of bounds

        ex.
        # original pixel locations
        i, j = np.meshgrid(np.arange(r), np.arange(c))
        ii = i.flatten()
        jj = j.flatten()
        flow = optical flow result from opencv
        get_flow_dst(ii,jj,flow)
        '''
        # note: this function is similar to Flow.apply_flow()
        r, c = flow.shape[0], flow.shape[1]
        ii = i
        jj = j

        # round flow to nearest neighbor // note: at far distances, this will cause large position error
        flow_int = flow.round().astype(int)
        flow_int_i = flow_int[:, :, 1].flatten()
        flow_int_j = flow_int[:, :, 0].flatten()

        # add flow
        new_i = ii + flow_int_i
        new_j = jj + flow_int_j
        print(ii.min(), ii.max(), jj.min(), jj.max())
        print(new_i.min(), new_i.max(), new_j.min(), new_j.max())


        # mask: True if within image frame; else False
        mask_i = (new_i >= 0) * (new_i < r) * (new_j >= 0) * (new_j < c)
        mask_j = (new_j >= 0) * (new_j < c) * (new_i >= 0) * (new_i < r)
        mask_i_invert = ~mask_i
        mask_j_invert = ~mask_j

        # apply mask: get True values only
        #masked_i = new_i[mask_i]
        #masked_j = new_j[mask_j]

        # for out of bounds, clip to image boundary
        # note: x = np.array([-3, -2, -1,  0,  1,  2,  3,  4,  5,  6]); neg = x<0; np.where(neg) = (array([0, 1, 2], dtype=int64),)
        new_i_clipped_negative_indices = np.where(new_i < 0)[0]
        new_j_clipped_negative_indices = np.where(new_j < 0)[0]
        new_i_clipped_positive_indices = np.where(new_i > (r-1))[0]
        new_j_clipped_positive_indices = np.where(new_j > (c-1))[0]
        new_i_clipped = new_i
        new_j_clipped = new_j
        new_i_clipped[new_i_clipped_negative_indices] = 0
        new_i_clipped[new_i_clipped_positive_indices] = r-1
        new_j_clipped[new_j_clipped_negative_indices] = 0
        new_j_clipped[new_j_clipped_positive_indices] = c-1


        # create out of bounds mask
        oob_mask = np.zeros((r,c))
        #oob_mask[mask_i_invert,mask_j_invert] = 1.0 # set out of bounds to white
        oob_mask = mask_j_invert & mask_j_invert
        oob_mask = oob_mask.reshape((c,r)).T.astype(np.float)

        # note: to use:
        # get point clouds corresponding to (i,j) and (new_i_clipped,new_j_clipped)
        return new_i_clipped, new_j_clipped, oob_mask

    def display(self):
        Z_L = self.flow_LR.Z
        Z_L_next = self.flow_LR_next.Z
        cv.imshow('Z Z_next',cv.normalize(np.hstack((Z_L,Z_L_next)),None,0,1.0,cv.NORM_MINMAX))
        cv.imshow('Z diff', cv.normalize(Z_L_next-Z_L, None, 0, 1.0, cv.NORM_MINMAX))
        flow_delta = np.abs(self.flow_LR.flow_forwards[:,:,0] - self.flow_LR_next.flow_forwards[:,:,0]) # static areas should be black; if not, this implies depth mismatch -> incorrect velocities
        if True: # set gray square for visual comparison
            flow_delta[0:100,0:100] = 0
        cv.imshow('flow_x delta', cv.normalize(flow_delta,None,0.0,1.0,cv.NORM_MINMAX))
        cv.imshow('L L_next', np.hstack((self.flow_LR.img_L,self.flow_LR_next.img_L)))
        if self.DISPLAY_OOB_MASK: cv.imshow('oob mask', self.oob_mask)
        cv.imshow('L to L_next flow', self.img_flow)
        cv.imshow('L to L_next flow xy', np.hstack((self.img_flow[:,:,-1],self.img_flow[:,:,-3])))
        cv.imshow('velocity map', self.velocity_img)
        Vx = self.velocity[:,:,2]
        Vy = self.velocity[:,:,1]
        Vz = self.velocity[:,:,0]
        _ = plt.hist(Vx.flatten(), histtype='step', bins=100, color='r', linewidth=5)
        _ = plt.hist(Vy.flatten(), histtype='step', bins=100, color='g', linewidth=5)
        _ = plt.hist(Vz.flatten(), histtype='step', bins=100, color='b', linewidth=5)
        if True: # set gray square for visual comparison
            Vx[0:100, 0:100] = 0
            Vy[0:100, 0:100] = 0
            Vz[0:100, 0:100] = 0
        V_xyz = np.concatenate((Vx,Vy,Vz),1) # manually scale here
        cv.imshow('velocity map xyz', cv.normalize(V_xyz,None,0,1.0,cv.NORM_MINMAX))
        cv.waitKey(1)


        self.plot_point_clouds()
        self.plot_flow_lines()
        return

    def plot_point_clouds(self):
        a = o3d.geometry.PointCloud()
        b = o3d.geometry.PointCloud()
        a.points = o3d.utility.Vector3dVector(self.flow_LR.point_cloud_metric)
        b.points = o3d.utility.Vector3dVector(self.flow_LR_next.point_cloud_metric)
        a = a.voxel_down_sample(0.02)
        b = b.voxel_down_sample(0.02)

        x1 = copy.deepcopy(a)
        x2 = copy.deepcopy(b)
        x1.paint_uniform_color([1, 0.706, 0])
        x2.paint_uniform_color([0, .4, 1])

        o3d.visualization.draw_geometries([x1, x2])
        self.o3d_pc_1 = x1
        self.o3d_pc_2 = x2
        return

    def plot_flow_lines(self):

        # pc, pc_next .shape: (c,r,3)
        pc = self.pc
        pc_next = self.pc_next
        N_match = len(pc)

        points = np.concatenate((pc,pc_next),axis=0) # [[x,y,z]_pc, [x,y,z]_pc_next]
        lines = [] # 0 -> N+0, 1 -> N+1, 2 -> N+2, ... +N-1
        for i in range(N_match): # 0..N-1
            lines.append([i, N_match + i])

        colors = [[1,0,0] for i in range(len(lines))] # red lines for all
        line_set = o3d.geometry.LineSet(
            points=o3d.utility.Vector3dVector(points),
            lines=o3d.utility.Vector2iVector(lines)
        )
        line_set.colors = o3d.utility.Vector3dVector(colors)
        o3d.visualization.draw_geometries([self.o3d_pc_1, self.o3d_pc_2, line_set])
        self.o3d_flow_lines = line_set
        # also, can append stuff to Visualizer() instead of calling draw_geometries() with all stuff
        # http://www.open3d.org/docs/release/tutorial/visualization/customized_visualization.html#mimic-draw-geometries-with-visualizer-class
        return

    def compute_flow_in_time_sparse(self):
        '''
        compute sparse optical flow of L[k]-->L[k+1] using ORB features
        where k is discrete time index
        L is left camera image
        '''
        if self.DEBUG_FLAG: print('computing sparse flow in time...')

        img_L_k = self.flow_LR.img_L
        img_L_k_plus = self.flow_LR_next.img_L
        dims = img_L_k.shape
        r = dims[0]
        c = dims[1]

        def calc(img_A, img_B):
            '''
            compute flow from A to B
            return list of indices from img_A to img_B: [((Ai,Aj),(Bi,Bj)),...]
            '''
            orb = cv.ORB_create()
            kp1, des1 = orb.detectAndCompute(img_A, None)
            kp2, des2 = orb.detectAndCompute(img_B, None)
            bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)
            matches = bf.match(des1, des2)
            matches = sorted(matches, key=lambda x: x.distance)

            # given a list of matches, get list of indices in the two images
            match_ij = []
            for i in range(len(matches)):
                j1, i1 = kp1[matches[i].queryIdx].pt
                j2, i2 = kp2[matches[i].trainIdx].pt
                i1 = np.array(i1).round().astype(int)
                j1 = np.array(j1).round().astype(int)
                i2 = np.array(i2).round().astype(int)
                j2 = np.array(j2).round().astype(int)
                if (i1 >= 0) & (i1 < r) & (j1 >= 0) & (j1 < c) & (i2 >= 0) & (i2 < r) & (j2 >= 0) & (j2 < c):
                    # at this point, can also add condition: if matches[i].distance < threshold, .append()
                    match_ij.append(((i1, j1), (i2, j2)))
            if self.DEBUG_FLAG: print('# matches found:', len(match_ij))

            img_orb = cv.drawMatches(img_A, kp1, img_B, kp2, matches[:], None,
                                     flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
            cv.imshow('orb', img_orb)
            cv.waitKey(1)

            img_flow = np.zeros((r,c,3)) # TODO

            return match_ij, img_flow

        flow_LtoLnext, img_flow = calc(img_L_k, img_L_k_plus)

        def get_idx(i,j):
            '''given pixel coordinates (i,j), get index for point_cloud_metric'''
            idx = c * i + j
            return idx  # linear index

        pc_next = np.stack((
            self.flow_LR_next.point_cloud_metric[:, 0].reshape(c, r),
            self.flow_LR_next.point_cloud_metric[:, 1].reshape(c, r),
            self.flow_LR_next.point_cloud_metric[:, 2].reshape(c, r)
        ), 2)
        pc = np.stack((
            self.flow_LR.point_cloud_metric[:, 0].reshape(c, r),
            self.flow_LR.point_cloud_metric[:, 1].reshape(c, r),
            self.flow_LR.point_cloud_metric[:, 2].reshape(c, r)
        ), 2)

        velocity = np.zeros((c,r,3))
        pc_match = []
        pc_next_match = []
        for k in range(len(flow_LtoLnext)):
            ((i1,j1),(i2,j2)) = flow_LtoLnext[k]
            velocity[j1,i1] = (pc_next[j2,i2] - pc[j1,i1])/self.Ts
            pc_match.append(pc[j1,i1])
            pc_next_match.append(pc_next[j2,i2])
        velocity = np.transpose(velocity, (1,0,2))
        velocity = np.flip(velocity,2) # RGB -> BGR for opencv
        velocity_normalized = cv.normalize(velocity, None, 0, 1.0, cv.NORM_MINMAX)
        # velocity_normalized = np.zeros((r,c,3)) # TODO: visualizing as binary motion for debug
        # velocity_normalized[velocity>0] = 1
        # velocity_normalized[velocity<0] = -1
        # velocity_normalized = cv.normalize(velocity_normalized, None, 0, 1.0, cv.NORM_MINMAX)
        # NOTE: velocity image will be corrupted by incorrect ORB matches; including long-distance matches which significantly affect image normalization

        self.velocity = velocity
        self.velocity_img = velocity_normalized
        self.sparse_flow = flow_LtoLnext
        self.img_flow = img_flow
        self.pc = pc_match # only contains points in match
        self.pc_next = pc_next_match # " "
        return
# END SceneFlow

# https://github.com/intel-isl/Open3D/blob/master/examples/python/pipelines/global_registration.ipynb
class PountCloudMatch:
    '''
    TODO: given stereo images at t=k and t=k-1, find camera motion R|t by point cloud matching // low priority
    '''
    def __init__(self):

        return

# END PountCloudMatch

class PointCloud:
    '''
    centroid, PCA, softmax inverse distance of kNN, ... -> feature vector v
    distance matrix: dist(v_i,v_j) for points i,j

    given a point cloud P with N points
    given a query point, find the closest point to it by: point index in P = argmin [||v_query-v_i|| for i in 1..N]

    ex.
    k = 100 # use 100 closest neighbors
    query_point_cloud = np.zeros(N,3) # XYZ
    query_values = np.zeros(N,3) # RGB
    x = PointCloud()
    x.compute_distance_matrix()
    x.get_kNN(k)
    index = x.find(query_point_cloud, query_values)
    '''
    def __init__(self, data, DEBUG_FLAG=False):
        '''
        data = {'points': point cloud locations; np.array of shape (N,3), 'value': RGB/gray value of point; shape (N,1 or 3)}
        location of i'th point: data['points'][i,:] -> [x,y,z]
        value of i'th point: data['value'][i] --> [r,g,b] or scalar
        '''
        self.DEBUG_FLAG = DEBUG_FLAG
        self.data = data
        self.N = data['points'].shape[0] # N
        self.kNN_list = []
        self.distance_matrix = self.compute_distance_matrix() # (N,N)
        self.features_list = []
        return

    def compute_distance_matrix(self):
        x = self.data['points']
        return distance_matrix(x, x) # scipy distance_matrix(x,y): x, y: Matrix of M vectors in K dimensions.

    def get_kNN(self, k):
        '''
        get k nearest neighbors of each point using distance matrix
        and store in kNN_list

        this includes the self point

        ex.
        kNN_list[4] contains indices corresponding to the k nearest neighbors for point 4

        4'th point corresponds to distance_matrix[4,:] or [:,4]
        '''

        # https://kanoki.org/2020/01/14/find-k-smallest-and-largest-values-and-its-indices-in-a-numpy-array/
        # k smallest values in x: np.partition(x,k)[:k]
        # indices of k smallest values in x: np.argpartition(x,k)[:k]
        # also, k smallest values in x: x[np.argpartition(x,k)[:k]]
        # note: result is in arbitrary order

        # for each row or column in distance matrix (aka. for each point)
        # find the indices of it's k nearest neighboring points (including itself)
        # and store in list
        self.kNN_list = []
        for i in range(self.N): # for each point
            curr_distances = self.distance_matrix[i,:] # can do [i,:] or [:,i], since symmetric
            kNN_indices = list(np.argpartition(curr_distances,k)[:k]) # list of k nearest neighbors
            self.kNN_list.append(kNN_indices)
        return

    @staticmethod
    def get_centroid(x):
        '''
        x.shape = (N,3)
        find centroid of N points in 3D space
        '''
        return np.sum(x,axis=0,keepdims=True) / 3

    @staticmethod
    def get_weighted_centroid(x,y):
        '''
        x.shape = (1,3) // self point
        y.shape = (N-1,3) // the k nearest neighbors of x
        compute centroid using softmax normalized inverse distance to x
        '''
        N1 = y.shape[0]
        distances = np.zeros(N1)
        for i in range(N1):
            distances[i] = np.linalg.norm(x.flatten()-y[i,:])

        out = PointCloud.softmax(1/distances)
        return np.expand_dims(out,axis=0)

    @staticmethod
    def softmax(X, theta=1.0, axis=None):
        """
        Compute the softmax of each element along an axis of X.

        Parameters
        ----------
        X: ND-Array. Probably should be floats.
        theta (optional): float parameter, used as a multiplier
            prior to exponentiation. Default = 1.0
        axis (optional): axis to compute values along. Default is the
            first non-singleton axis.

        Returns an array the same size as X. The result will sum to 1
        along the specified axis.

        https://nolanbconaway.github.io/blog/2017/softmax-numpy.html
        """

        # make X at least 2d
        y = np.atleast_2d(X)

        # find axis
        if axis is None:
            axis = next(j[0] for j in enumerate(y.shape) if j[1] > 1)

        # multiply y against the theta parameter,
        y = y * float(theta)

        # subtract the max for numerical stability
        y = y - np.expand_dims(np.max(y, axis=axis), axis)

        # exponentiate y
        y = np.exp(y)

        # take the sum along the specified axis
        ax_sum = np.expand_dims(np.sum(y, axis=axis), axis)

        # finally: divide elementwise
        p = y / ax_sum

        # flatten if X was 1D
        if len(X.shape) == 1: p = p.flatten()

        return p

    '''def construct_feature(self): # TODO-ing <-----------
        # using self.data point cloud
        # 1) for each point, get it's kNN // get_kNN()
        # 2) then, compute the centroid
        # 3) create feature

        features_list = []
        for i in range(self.N):
            indices = self.kNN_list[i]
            points = self.data['points'][indices] # (k,3)
            centroid = PointCloud.get_centroid(points) # (1,3)
            centered_points = points - centroid # (k,3)

            # calculate kPCA of centered_points and save coefficients
            kpca = KernelPCA(kernel='poly', n_components=4) # TODO: reminder: copy_X=False to save memory, assuming data is static
            x_transformed = kpca.fit_transform(centered_points)
            coef = kpca.lambdas_ # (n_components,)

            # calculate histogram of self.data['value'][indices]
            hist, edges = np.histogram(self.data['value'][indices], bins=64, range=(0.0,1.0), density=True) # hist.shape = (bins,)

            # create dict for calculating features
            v = {'kpca': kpca, 'v': x_transformed, 'hist': hist}

            # save for this point
            features_list.append(v)

            # given query points (k,3) with feature vector v, find closest point in point cloud: i = argmin [||v-v_i||] for i in 0..N
            # v = concatenate: kpca.fit_transform(query points) , hist
            # and
            # v_i = concatenate: self.features_list['x_transformed'] , self.features_list['hist']

            # still not correct TODO

        self.features_list = features_list

        return'''

    def find(self, query_point_cloud, query_values):
        '''
        assuming point clouds have nodes (XYZ) and associated value(s) (gray/RGB),
        given a query set of nodes and values, find the best matching node in
        self.data, which contains nodes and values

        rephrase:
        given a point cloud P with N points
        given a query point, find the closest point to it by: point index in P = argmin [||v_query-v_i|| for i in 1..N]

        query_point_cloud (kNN from another scene): (Nq,3)
        compute best match of query_point_loud with each kNN point cloud in self.data['points']
        using self.kNN_list

        query_value (kNN from another scene): (Nq,1 or 3) // gray/RGB pixel values

        return index of best match
        '''

        def get_color_feature(query_value, mode='L2'):
            '''
            given a list of colors; np array (N,3), np array (N,1), or [(3,), (3,), ...]
            return histogram feature
                        '''
            x = Octree()
            # add points to x
            for point in query_value:
                f = x.get_feature(point, flatten=True) # if value is scalar, get_feature(is_scalar=True)
                x.update(f)
            x.finalize(normalize=True)

            if mode=='L0':
                out = x.histogram_hamming
            else: # L1/L2
                out = x.histogram

            if self.DEBUG_FLAG: print('color feature shape:', out.shape)
            return out

        # compute fpfh feature of query
        voxel_size = 0.050  # [meter]
        x0 = PointCloudFeature()
        x0.set_point_cloud(query_point_cloud)
        x0.prepare_dataset(voxel_size)
        fpfh_query = x0.get_feature_vector().flatten()
        if self.DEBUG_FLAG:
            print('query point cloud shape:', query_point_cloud)
            print('fpfh query (geometry feature) shape:', fpfh_query.shape)

        # compute histogram feature of query
        feature_query = get_color_feature(query_values)

        fpfh_list = []
        cost_list = []
        for i in range(self.N):
            target_indices = self.kNN_list[i] # list of k nearset neighbors of i
            target_point_cloud = self.data['points'][target_indices] # xyz coordinates of kNN
            target_values = self.data['value'][target_indices] # rgb/gray value of kNN

            if self.DEBUG_FLAG: print('target point cloud shape:', target_point_cloud.shape)

            # don't do for now: ransac -> ICP: try to align query_point_cloud to target_point_cloud
            # return i with best matching score/cost

            # instead
            # calculate fpfh geometry feature and save to list
            x = PointCloudFeature()
            x.set_point_cloud(target_point_cloud)
            x.prepare_dataset(voxel_size)
            fpfh = x.get_feature_vector().flatten() # matrix -> flatten to vector
            if self.DEBUG_FLAG: print('fpfh (geometry feature) shape:', fpfh.shape)
            fpfh_list.append(fpfh)

            # compute geometry L2 distance
            cost_geometry = np.linalg.norm( fpfh_query - fpfh )

            # compute color feature
            feature_target = get_color_feature(target_values)

            # compute color L0 distance
            cost_color = np.sum(np.abs( feature_query - feature_target ))/self.N

            # weigh # TODO: calculate histogram of cost_geometry and cost_color; then decide cost weighting alpha <--------------
            # ex. alpha = 1: {'cost_total': 1706.9270187720074, 'cost_geometry': 1706.926756362902, 'cost_color': 0.0002624091052797977}
            alpha = 1.0
            cost_total = cost_geometry + alpha*cost_color
            cost_dict = {'cost_total':cost_total, 'cost_geometry':cost_geometry, 'cost_color':cost_color}
            cost_list.append(cost_dict)

        # get best matching
        temp = np.array([x['cost_total'] for x in cost_list]) # get all 'cost_total' values from list of dicts
        #min_cost = min(temp)
        #min_index = cost_list.index(temp) # argmin; return first minimum if multiple identical global minima
        min_index = temp.argmin()
        min_cost = temp[min_index]

        print('cost dict:', cost_list) # TODO: turn off this print when finished debugging
        print('min. cost:', min_cost)
        print('min. index:', min_index)

        return min_index # TODO: best N matches, where N is an arguement

    def find_closest_point(self, query_point):
        '''
        query_point: (1,3)
        find closest point that query_point has to all N points in point cloud
        using min [||query point feature - i'th point feature|| for i in 1..N]
        '''
        return

    @staticmethod
    def compute_flow(source_point, destination_point):
        '''
        compute a point's flow
        '''
        return destination_point - source_point

# END PointCloud

class PointCloudFeature():
    '''
    compute fpfh feature vectors for a point cloud

    ex.
    voxel_size = 0.050 # [meters]
    point_cloud = np.zeros((N,3))
    x = PointCloudFeature()
    x.set_point_cloud(point_cloud)
    x.prepare_dataset(voxel_size)
    fpfh = x.get_feature_vector() # (33,N)
    '''
    def __init__(self):
        self.point_cloud = None
        self.voxel_size = None
        self.down = None
        self.fpfh = None
        return

    def set_point_cloud(self, point_cloud):
        self.point_cloud = point_cloud
        return

    @staticmethod
    def preprocess_point_cloud(pcd, voxel_size): # don't use voxel downsampling b/c this sets the downsampled point cloud #points non-deterministically (depends on data)
        #pcd_down = pcd.voxel_down_sample(voxel_size)
        pcd_down = pcd # don't downsample; do this at the beginning: downsample -> PointCloud -> PointCloudFeature
        #radius_normal = voxel_size * 2
        radius_normal = 0.050 * 2 # [meters]
        pcd_down.estimate_normals(o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))
        radius_feature = voxel_size * 5
        pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(pcd_down, o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
        return pcd_down, pcd_fpfh

    def prepare_dataset(self, voxel_size):

        pc = o3d.geometry.PointCloud()
        pc.points = o3d.utility.Vector3dVector(self.point_cloud)

        down, fpfh = self.preprocess_point_cloud(pc, voxel_size)

        self.voxel_size = voxel_size
        self.down = down
        self.fpfh = fpfh
        return

    def get_feature_vector(self):
        return self.fpfh.data # (33,N)

# END PointCloudFeature


if __name__=="__main__":
    if True:
        x = Flow()
        x.compute_flow(data_index=88+10, resize_factor=0.2)
        x.apply_flow_to_img()
        x.display()
        x.calc_interpolation_error()


        p = camera.GetCameraParameters('g85 25mm')
        p.info()
        c = camera.Camera(p.get())
        x.calc_depth(c)
        x.backproject()
        #z = cv.normalize(x.Z, None, 0, 1.0, cv.NORM_MINMAX) # need to remove outliers
        cv.imshow('z', x.Z/1e7*3*3) # ignore outliers and over/under-flow for now
        cv.waitKey(1)
        # x.display_point_cloud() # this line should be last; pyvista can't close...



    if False:
        temp = x.Z.flatten()
        #temp = temp * (np.abs(temp)<1e8)
        temp = x.Z.flatten() * x.camera.pixel_pitch_x / 1e6 # [px]->[m]
        #temp = temp * (np.abs(temp)<1e3)
        _=plt.hist(temp,histtype='step',bins=1000)
        plt.yscale('log')
        plt.xscale('symlog')
        plt.grid(True)
        plt.show()

    if True:
        #------------------- test PointCloud ------------------------ # TODO <
        decimation_factor = 100
        point_cloud = x.point_cloud_metric[::100,:] # (r*c,3) [meters]
        values = x.img_RGB_L
        values = np.transpose(values,(1,0,2)).reshape((values.shape[0]*values.shape[1],3)).astype(np.float)/255 # uint8 to float 0..1 for color feature calculation
        data = {'points':point_cloud, 'value':values}
        k = 100 # ex. 10% of number of points in the scene
        y = PointCloud(data, DEBUG_FLAG=False)
        y.compute_distance_matrix()
        y.get_kNN(k)

        # get query_point_cloud and query_values from one point in x.point_cloud_metric
        i = 99
        query_point_cloud_indices = y.kNN_list[i] # i'th point
        query_point_cloud = y.data['points'][query_point_cloud_indices]
        query_values = y.data['value'][query_point_cloud_indices]

        # find index of best matching; should equal i
        index = y.find(query_point_cloud, query_values)
        # TODO <========= run and test

    if False:
        # test SceneFlow.get_flow_dst()
        ii, jj = np.meshgrid(np.arange(5), np.arange(8))
        ii, jj = ii.flatten(), jj.flatten()
        flow_x = np.array([
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]) *-2
        flow_y = np.array([
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]) *1
        flow = np.stack((flow_x,flow_y),-1)
        i_new, j_new, oob_mask = SceneFlow.get_flow_dst(ii, jj, flow)
        #print(i_new.reshape((8,5)).T - ii.reshape((8,5)).T)
        #print(j_new.reshape((8,5)).T - jj.reshape((8,5)).T)
        print(oob_mask)
        cv.imshow('oob_mask x10', cv.resize(oob_mask, tuple(reversed([int(x * 10) for x in oob_mask.shape]))))
        cv.waitKey(1)

        # test velocity
        flow_x = np.zeros((5,8))
        flow_y = np.zeros((5,8))
        flow_x[0,0] = 1
        flow = np.stack((flow_x, flow_y), -1)
        i_new, j_new, oob_mask = SceneFlow.get_flow_dst(ii, jj, flow)
        pc = np.zeros((40,3))
        #pc[1,:] += 1
        pc_next = np.zeros((40,3))
        pc_next[1,:] += 1
        def get_idx(i,j,r,c):
            return c*i + j
        idx_1 = get_idx(ii,jj,5,8)
        idx_2 = get_idx(i_new,j_new,5,8)
        velocity = pc_next[idx_2,:] - pc[idx_1,:]
        velocity2 = np.linalg.norm(velocity,axis=-1).reshape((8,5)).T# magnitude
        print(velocity2)
        print(np.sqrt(3))
        for a,b,c,d,e in zip(idx_1,idx_2,pc[idx_1,0],pc_next[idx_2,0], pc_next[idx_2,0]-pc[idx_1,0]): print(a,b,c,d,e)


    if False:
        scene_data_dict = {
            'current': 88,
            'next': 98
        }
        scene = SceneFlow(data_index=scene_data_dict, display=True, DEBUG_FLAG=True)

        #def plot_point_clouds():
        #    a = o3d.geometry.PointCloud()
        #    b = o3d.geometry.PointCloud()
        #    a.points = o3d.utility.Vector3dVector(scene.flow_LR.point_cloud_metric)
        #    b.points = o3d.utility.Vector3dVector(scene.flow_LR_next.point_cloud_metric)
        #    a = a.voxel_down_sample(0.02)
        #    b = b.voxel_down_sample(0.02)
#
        #    x1 = copy.deepcopy(a)
        #    x2 = copy.deepcopy(b)
        #    x1.paint_uniform_color([1, 0.706, 0])
        #    x2.paint_uniform_color([0,.4,1])
#
        #    o3d.visualization.draw_geometries([x1,x2])
        #    return
        #plot_point_clouds()


    print('-- done')




# IGNORE: calculate forwards-backward endpoint error // see "Estimation and Analysis of Motion in Video Data - see forwards backwards endpoint error.pdf" <----------------
# for optical flow function f_LR (forwards flow) and f_RL (backwards flow), error = ||f_RL( x + f_LR(x) )||
# endpoint error (EE) requires ground truth flow vectors
#
# instead, use interpolation error (IE) // TODO calculate interpolation error
# A <-> B, A_flowed, B_flowed
# IE_forwards = ||A_flowed - B||
# IE_backwards = ||B_flowed - A||
# probably should inpaint
# or calculate using mask:
# IE_forwards = ||img_L_flowed_mask*img_L_flowed - img_L_mask*img_R||

# DONE: copy/paste camera class to here, do inverse projection and find depth map <-------
# DONE: re-take pictures @ lens focal lenth with 0 distortion
# ex. assume L is reference camera
# use img_L and img_L_flowed_inpainted
# with camera paramters and project to depths
# project @ img_L_mask : img_L
# project @ img_L_flowed_mask : img_L_flowed
# ------------------------------------------------------------------
# easy way for small (1cm) displacement:
# disparity = B*f/Z
#
# disparity: x-flow (px)
# B: x-distance between cameras (px)
# f: focal length (px)
# Z: depth (px)

# // fill holes (inpainting) // https://towardsdatascience.com/image-inpainting-with-a-single-line-of-code-c0eef715dfe2
# res_NS = cv2.inpaint(distort_img, mask, 3, cv2.INPAINT_NS)
# res_TELEA = cv2.inpaint(distort_img, mask, 3, cv2.INPAINT_TELEA)
# res_FSRFAST = distorted_img.copy()
# res_FSRBEST = distorted_img.copy()
# mask1 = cv2.bitwise_not(mask)
# cv2.xphoto.inpaint(distort, mask1, res_FSRFAST, cv2.xphoto.INPAINT_FSR_FAST)
# cv2.xphoto.inpaint(distort, mask1, res_FSTBEST, cv2.xphoto.INPAINT_FSR_BEST)
#
# cv.imshow('',cv.inpaint(new_img,cv.bitwise_not(new_img_mask), 3, cv.INPAINT_TELEA))
# cv.xphoto.inpaint(new_img, new_img_mask, temp, cv.xphoto.INPAINT_FSR_FAST)

# notes:
# optical flow does not work well in plain areas (no texture/edges/features)
# optical flow does not work well for large camera displacements, especially if features rotate (close distance)
# therefore, want:
# filter/mask out plain areas
# use small (1cm) camera displacement for close working distances
# use large (6cm) camera displacement for large working distances

# notes/summary:
# ground plane could be distorted; need features to help optical flow algorithm //#7 floor 1cm
# need unique textures; othersize can get bad flow // #9 drawer 1cm
# horizontal railing: optical flow cannot detect features on railing, therefore no flow // #19 window 1cm
# optical flow not perfect at ray tracing through mirrors; it can, but is buggy
# optical flow has trouble with translucent objects // #35, 37
# minimum distance is defined by optical flow search radius and baseline
#   for large focal lengths at close distances, large baseline -> shift > search radius -> fail
# maximum distance is defined by camera calibration (mechanical/software) and baseline (larger is better for far objects)(optical flow precision)

# TODO: find ground plane // make new file to test
# given a blob of points, find 2 largest eigenvalues to get ground plane vectors; calculate unit normal "e" to this, point in opposite direction of gravity vector
# then, MPC xT (X,Y,Z): e*h_cog

# point cloud to mesh (surface reconstruction)
# https://github.com/pyvista/pyvista-support/issues/170#issuecomment-639211683

# note: point cloud registration (for calculating point cloud flow)
# but, this only works for static point clouds
# http://www.open3d.org/docs/release/tutorial/pipelines/global_registration.html

# DONE: stereo vision: search on epipolar line: try vertically stretching the images (so optical flow algorithm can't see as far vertically), (then calc. optical flow, ...), then vertically shrink the disparity/depth maps // results not good

# TODO: check sign of velocity <========================
# TODO: mask out points that appeared: L->L_next; try using z-buffer

# DONE for ORB: draw lines/arrows in point cloud to see flow

# given idx_1, idx_2 # linear src,dst indices
# occlusion_mask = ones(r*c) // True
# for i in range(r*c): # for all points
#   if any idx_1 == idx_2[i], excluding idx_1[i]: // if any source indices (excluding i) map to destination index i
#       occlusion_mask[idx_2] = False

# https://docs.opencv.org/3.4/dc/dd3/tutorial_gausian_median_blur_bilateral_filter.html # median filter
# https://scikit-image.org/docs/0.3/auto_examples/plot_lena_tv_denoise.html # total variation denoising

