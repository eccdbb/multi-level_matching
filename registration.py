import open3d as o3d
import numpy as np
import pathlib
import copy
import time

class Registration():
    def __init__(self):
        self.voxel_size = 1.0
        self.point_cloud_source = np.zeros((0,3))
        self.point_cloud_target = np.zeros((0,3))
        self.result_ransac = None
        self.result_ICP = None
        self.result_fast_feature_matching = None
        return

    def set_point_cloud_source(self,point_cloud):
        '''
        point_cloud: numpy array of shape (N,3)
        '''
        self.point_cloud_source = point_cloud
        return

    def set_point_cloud_target(self,point_cloud):
        '''
        point_cloud: numpy array of shape (N,3)
        '''
        self.point_cloud_target = point_cloud
        return

    @staticmethod
    def draw_point_cloud(point_cloud):
        x = copy.deepcopy(point_cloud)
        x.paint_uniform_color([1, 0.706, 0])
        o3d.visualization.draw_geometries([x],
                                          zoom=0.4559,
                                          front=[0.6452, -0.3036, -0.7011],
                                          lookat=[1.9892, 2.0208, 1.8945],
                                          up=[-0.2779, -0.9482, 0.1556])
        return

    def draw_source_target(self, downsample=True):
        if downsample:
            Registration.draw_point_cloud(self.source_down)
            Registration.draw_point_cloud(self.target_down)
        else:
            Registration.draw_point_cloud(self.source)
            Registration.draw_point_cloud(self.target)
        return

    @staticmethod
    def draw_registration_result(source, target, transformation):
        source_temp = copy.deepcopy(source)
        target_temp = copy.deepcopy(target)
        source_temp.paint_uniform_color([1, 0.706, 0])
        target_temp.paint_uniform_color([0, 0.651, 0.929])
        source_temp.transform(transformation)
        o3d.visualization.draw_geometries([source_temp, target_temp],
                                          zoom=0.4559,
                                          front=[0.6452, -0.3036, -0.7011],
                                          lookat=[1.9892, 2.0208, 1.8945],
                                          up=[-0.2779, -0.9482, 0.1556])
        return

    @staticmethod
    def preprocess_point_cloud(pcd, voxel_size):
        print("Downsample with a voxel size %.3f." % voxel_size)
        pcd_down = pcd.voxel_down_sample(voxel_size)

        radius_normal = voxel_size * 2
        print(":: Estimate normal with search radius %.3f." % radius_normal)
        pcd_down.estimate_normals(
            o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

        radius_feature = voxel_size * 5
        print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
        pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(pcd_down, o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
        return pcd_down, pcd_fpfh

    def prepare_dataset(self,voxel_size):
        '''
        load numpy point clouds
        '''
        # http://www.open3d.org/docs/release/tutorial/geometry/working_with_numpy.html
        source = o3d.geometry.PointCloud()
        target = o3d.geometry.PointCloud()
        source.points = o3d.utility.Vector3dVector(self.point_cloud_source)
        target.points = o3d.utility.Vector3dVector(self.point_cloud_target)

        source_down, source_fpfh = Registration.preprocess_point_cloud(source, voxel_size)
        target_down, target_fpfh = Registration.preprocess_point_cloud(target, voxel_size)

        self.voxel_size = voxel_size
        self.source = source
        self.target = target
        self.source_down = source_down
        self.target_down = target_down
        self.source_fpfh = source_fpfh
        self.target_fpfh = target_fpfh
        return

    def execute_global_registration(self, PRINT_FLAG=False):
        source_down = self.source_down
        target_down = self.target_down
        source_fpfh = self.source_fpfh
        target_fpfh = self.target_fpfh
        voxel_size = self.voxel_size

        distance_threshold = voxel_size * 1.5
        print("RANSAC registration on downsampled point clouds...")
        #print("   Since the downsampling voxel size is %.3f," % voxel_size)
        #print("   we use a liberal distance threshold %.3f." % distance_threshold)
        result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(
            source_down, target_down, source_fpfh, target_fpfh, distance_threshold,
            o3d.pipelines.registration.TransformationEstimationPointToPoint(False),
            4,
            [o3d.pipelines.registration.CorrespondenceCheckerBasedOnEdgeLength(0.9),
                o3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(distance_threshold)],
            o3d.pipelines.registration.RANSACConvergenceCriteria(4000000, 500))

        if PRINT_FLAG: print(result)
        self.result_ransac = result
        return

    def draw_global_registration(self):
        print('showing: global registration')
        Registration.draw_registration_result(self.source_down,self.target_down,self.result_ransac.transformation)
        return

    def refine_registration(self, PRINT_FLAG=False):
        source = self.source
        target = self.target
        voxel_size = self.voxel_size
        result_ransac = self.result_ransac

        # compute normals for target point cloud; ICP point to plane method needs this
        target.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))

        distance_threshold = voxel_size * 0.4
        print("Applying point-to-plane ICP registraion on original point cloud...")
        #print("Point-to-plane ICP registration is applied on original point...")
        #print("   clouds to refine the alignment. This time we use a strict")
        #print("   distance threshold %.3f." % distance_threshold)
        result = o3d.pipelines.registration.registration_icp(
            source, target, distance_threshold, result_ransac.transformation,
            o3d.pipelines.registration.TransformationEstimationPointToPlane()) # DONE: RuntimeError: [Open3D ERROR] TransformationEstimationPointToPlane and TransformationEstimationColoredICP require pre-computed normal vectors for target PointCloud

        if PRINT_FLAG: print(result)
        self.result_ICP = result
        return

    def draw_refined_registration(self, draw_correspondences_only=False):
        print('showing: global -> refined registration')
        if draw_correspondences_only:
            print('drawing correspondences only')
            ids = np.asarray(self.result_ICP.correspondence_set) # (N,2)
            corresponding_points_source = np.asarray(self.source.points)[ids[:,0]]
            corresponding_points_target = np.asarray(self.target.points)[ids[:,1]]
            source = o3d.geometry.PointCloud()
            target = o3d.geometry.PointCloud()
            source.points = o3d.utility.Vector3dVector(corresponding_points_source)
            target.points = o3d.utility.Vector3dVector(corresponding_points_target)

            Registration.draw_registration_result(source, target, self.result_ICP.transformation)
        else:
            Registration.draw_registration_result(self.source,self.target,self.result_ICP.transformation)
        return

    def execute_fast_global_registration(self, PRINT_FLAG=False):
        source_down = self.source_down
        target_down = self.target_down
        source_fpfh = self.source_fpfh
        target_fpfh = self.target_fpfh
        voxel_size = self.voxel_size

        distance_threshold = voxel_size * 0.5
        print("Apply fast global registration with distance threshold %.3f" % distance_threshold)
        result = o3d.pipelines.registration.registration_fast_based_on_feature_matching(
            source_down, target_down, source_fpfh, target_fpfh,
            o3d.pipelines.registration.FastGlobalRegistrationOption(maximum_correspondence_distance=distance_threshold))

        if PRINT_FLAG: print(result)
        self.result_fast_feature_matching = result
        return

    def draw_fast_global_registration(self):
        print('showing: fast global registration')
        Registration.draw_registration_result(self.source_down,self.target_down,self.result_fast_feature_matching.transformation)
        return


if __name__=="__main__":
    voxel_size = 0.05 # units depends on data; use meters; then, 0.05 -> 5cm
    x = Registration()

    # load .pcd files and extract numpy matrix
    source_path = str(pathlib.Path('./test_data/cloud_bin_0.pcd'))
    target_path = str(pathlib.Path('./test_data/cloud_bin_1.pcd'))
    source = o3d.io.read_point_cloud(source_path)
    target = o3d.io.read_point_cloud(target_path)
    source = np.asarray(source.points) # N,3
    target = np.asarray(target.points) # N,3

    # load from flow.py generated point clouds
    def get_point_cloud(id:int, filter=True):
        '''
        id: integer
        '''
        file = 'point_cloud_metric_'+str(id)+'.npz'
        print('loading:', file)
        data = np.load(file)
        if filter:
            print('using filtered point cloud')
            out = data['point_cloud_metric']
            max_distance_threshold = 10 # meters
            out = out[np.linalg.norm(out,axis=1)<max_distance_threshold]
            min_distance_threshold = 0 # meters
            out = out[np.linalg.norm(out,axis=1)>min_distance_threshold]
        else:
            out = data['point_cloud_metric']
        return out

    source = None
    target = None
    # set A: 47, 49
    source = get_point_cloud(47)
    target = get_point_cloud(49)
    # set B: 51, 53, 55, 57
    source = get_point_cloud(53)
    target = get_point_cloud(57)
    # set C: 59, 61, 63
    source = get_point_cloud(59)
    target = get_point_cloud(63)
    # set D: 64, 65, 67, 68, 69
    source = get_point_cloud(67)
    target = get_point_cloud(68) # 67,69 fast global registration // works
    # set E: 88, 90, 92, 94, 96, 98 // use these for scene flow
    source = get_point_cloud(88)
    target = get_point_cloud(90)

    # set point cloud data
    x.set_point_cloud_source(source)
    x.set_point_cloud_target(target)

    # prepare
    x.prepare_dataset(voxel_size)

    # method 1) a) coarse global -> b) refined registration
    x.execute_global_registration(PRINT_FLAG=True)
    x.refine_registration(PRINT_FLAG=True)

    # method 2) fast global registration
    x.execute_fast_global_registration(PRINT_FLAG=True)

    # draw results
    x.draw_global_registration()
    x.draw_refined_registration()
    x.draw_refined_registration(draw_correspondences_only=True)
    x.draw_fast_global_registration()
    x.draw_source_target(downsample=False)

    # to get (4,4) transformation matrix, do:
    # x.result_ransac/ICP/fast_feature_matching.transformation
    #
    # example:
    # array([[0.84048192, 0.00662211, -0.54179912, 0.64521371],
    #        [-0.14732808, 0.96504563, -0.21675184, 0.8094618],
    #        [0.52142552, 0.26199823, 0.81207903, -1.48457032],
    #        [0., 0., 0., 1.]])

    # extract fpfh features and see what datatype it is
    print(x.source_fpfh.data.shape) # numpy array; (33,N) ; ex. (33, 2851)
    print(np.asarray(x.source_down.points).shape) # (N,3) ; ex. (2851, 3)
    # TODO: create class similar to Registration, and output fpfh of one point cloud <===============================


    print('-- done')

    # TODO: filter point cloud by correspondence
    # given a set of point clouds (taken from different views, etc.): {pc_1, pc_2, pc_3, pc_4, ...}
    # initialize an empty point cloud pc_aggregated (shape=(0,3))
    # for i in range nCr (take 2 where order doesn't matter):
    #   take a pair, perform registration
    #   if successful fit, get correspondence set
    #   add corresponding target and transformed source to pc_aggregated
    # END

    # TODO: augment FPFH feature with color --> joint geometry-color optimization during point cloud flow (scene flow) calculation
    # use normalized histogram of colors -> convert the value of each bin to binary number; ex. 10 bins, 6 bits/bin -> 60 bit color feature vector
    # see: def colorhash() @ https://github.com/JohannesBuchner/imagehash/blob/master/imagehash.py#L327
    # ||source-target||_2_(color feature) = hamming distance (# of different bits)
    # TODO: constrained ICP using expected IMU (inertial measurement unit) [R|t] and drift ball // using Intel RealSense

    # TODO: really need cleaner point clouds; for now, find a good scene and do: fixed camera,  moving objects