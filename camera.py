import numpy as np
#import tensorflow as tf

class GetCameraParameters:
    def __init__(self, camera_name: str):
        '''
        get a base set of camera parameters
        (then deepycopy, modify, and put into CameraSet)
        '''
        if (camera_name=='g85 25mm'): # use this as reference for how to set camera_properties_dict
            x_resolution = 4592 # px; integer
            y_resolution = 3448 # px; integer
            sensor_width = 17.3 * 1000 # um
            sensor_height = 13 * 1000 # um
            pitch = sensor_width / x_resolution # pixel pitch; um / px
            
            camera_properties_dict = {}
            
            # sensor
            camera_properties_dict['pp_x'] = x_resolution/2 + 0.5 # px; principal point x
            camera_properties_dict['pp_y'] = y_resolution/2 + 0.5 # px; principal point y
            camera_properties_dict['pixel_pitch_x'] = pitch # um/px; pixel pitch x
            camera_properties_dict['pixel_pitch_y'] = pitch # um/px; pixel pitch y
            camera_properties_dict['N_x'] = x_resolution
            camera_properties_dict['N_y'] = y_resolution
            
            # pose
            camera_properties_dict['theta'] = np.array([[0,0,0]],dtype=np.float32).T # pose; angle (radians) (3,1)
            camera_properties_dict['delta'] = np.array([[0,0,0]],dtype=np.float32).T # pose; position [px] (3,1)

            # lens
            camera_properties_dict['f'] = 25. / pitch * 1000 # focal length [mm]->[px]
            camera_properties_dict['distortion_coef'] = np.array([0,0,0,0,0],dtype=np.float32)
            camera_properties_dict['undistortion_coef'] = np.array([0,0,0,0,0],dtype=np.float32)

        elif (camera_name=='g85 100mm'):
            x_resolution = 4592  # px; integer
            y_resolution = 3448  # px; integer
            sensor_width = 17.3 * 1000  # um
            sensor_height = 13 * 1000  # um
            pitch = sensor_width / x_resolution  # pixel pitch; um / px

            camera_properties_dict = {}

            # sensor
            camera_properties_dict['pp_x'] = x_resolution / 2 + 0.5  # px; principal point x
            camera_properties_dict['pp_y'] = y_resolution / 2 + 0.5  # px; principal point y
            camera_properties_dict['pixel_pitch_x'] = pitch  # um/px; pixel pitch x
            camera_properties_dict['pixel_pitch_y'] = pitch  # um/px; pixel pitch y
            camera_properties_dict['N_x'] = x_resolution
            camera_properties_dict['N_y'] = y_resolution

            # pose
            camera_properties_dict['theta'] = np.array([[0, 0, 0]], dtype=np.float32).T  # pose; angle (radians) (3,1)
            camera_properties_dict['delta'] = np.array([[0, 0, 0]], dtype=np.float32).T  # pose; position [px] (3,1)

            # lens
            camera_properties_dict['f'] = 100. / pitch * 1000  # focal length [mm]->[px]
            camera_properties_dict['distortion_coef'] = np.array([0, 0, 0, 0, 0], dtype=np.float32)
            camera_properties_dict['undistortion_coef'] = np.array([0, 0, 0, 0, 0], dtype=np.float32)

        elif(camera_name=='KITTI'):
            # Sony ICX267AK
            # Diagonal 8mm (Type1/2) Progressive Scan CCD Image Sensor with Square Pixel for Color Cameras
            x_resolution = 1392 # same for all cameras; see KITI calib_cam_to_cam.txt
            y_resolution = 512
            pitch = 4.65 # um / px # see Sony ICX267AK datasheet

            camera_properties_dict = {}

            # sensor
            # set later b/c different for each camera
            camera_properties_dict['pp_x'] = np.inf # # px; principal point x
            camera_properties_dict['pp_y'] = np.inf  # px; principal point y
            camera_properties_dict['pixel_pitch_x'] = pitch  # um/px; pixel pitch x
            camera_properties_dict['pixel_pitch_y'] = pitch  # um/px; pixel pitch y
            camera_properties_dict['N_x'] = x_resolution
            camera_properties_dict['N_y'] = y_resolution

            # pose
            camera_properties_dict['delta'] = np.array([[np.inf,np.inf,np.inf]],dtype=np.float32).T  # pose; angle (radians) (3,1) from camera 0 to i
            camera_properties_dict['theta'] = np.array([[np.inf,np.inf,np.inf]],dtype=np.float32).T # hint: convert from rotation matrix to angles; use MATLAB rotm2eul() or eul2rotm()
            # TODO: how to rotate-translate from camera 02 to 03? for now, just use camera 00 as cameras axis/orign definition and set poses of {02,03}
            # set pose later

            # lens
            camera_properties_dict['f'] = np.inf # [px]; from K (unrectified/unsynced) # reminder: focal length (mm) =  focal length (px) *pitch/1000
            camera_properties_dict['distortion_coef'] = np.array([-0.37914,0.21481,0.0012271,0.0023438,-0.079104],dtype=np.float32) # from KITTI cam_to_cam.txt
            camera_properties_dict['undistortion_coef'] = np.array([0.4431,-0.23938,-0.0037364,-0.0063101,0.79991],dtype=np.float32) # run camera_distortion_test.m to get this

            # summary of values that should be set later: {pp_x, pp_y, delta, theta, f}
        else:
            camera_properties_dict = {}
            print('null camera')
        
        camera_properties_dict['name'] = camera_name

        # set property
        self.camera_properties_dict = camera_properties_dict
        return

    def get(self):
        return self.camera_properties_dict

    def info(self):
        print(self.camera_properties_dict)
        return
# END GetCameraParameters
            

class Camera:
    def __init__(self,camera_properties_dict):
        self.name = camera_properties_dict['name']

        # sensor
        self.pp_x = camera_properties_dict['pp_x']
        self.pp_y = camera_properties_dict['pp_y']
        self.pixel_pitch_x = camera_properties_dict['pixel_pitch_x']
        self.pixel_pitch_y = camera_properties_dict['pixel_pitch_y']
        self.N_x = camera_properties_dict['N_x']
        self.N_y = camera_properties_dict['N_y']

        # lens
        self.f = camera_properties_dict['f']
        self.distortion_coef = camera_properties_dict['distortion_coef']
        self.undistortion_coef = camera_properties_dict['undistortion_coef']

        # pose
        self.theta = camera_properties_dict['theta']
        self.delta = camera_properties_dict['delta']

        self.update()
        return

    def info(self):
        print('name = ', self.name)

        print('pp_x = ', self.pp_x)
        print('pp_y = ', self.pp_y)
        print('pixel_pitch_x = ', self.pixel_pitch_x)
        print('pixel_pitch_y = ', self.pixel_pitch_y)
        print('N_x = ', self.N_x)
        print('N_y = ', self.N_y)

        print('f = ', self.f)

        print('theta = \n', self.theta)
        print('delta = \n', self.delta)
        return

    def update(self):
        '''after modifying camera, run update()'''

        # create intrinsic matrix
        self.K = np.array([
            [self.f,0,self.pp_x],
            [0,self.f,self.pp_y],
            [0,0,1.0]
        ])

        # create inverse intrinsic matrix
        self.K_inv = Camera.calc_K_inv(self.f,self.pp_x,self.pp_y)

        # create transform
        self.R , self.t = self.calc_rotation_translation_matrix(self.theta,self.delta)

        # calc. camera matrix // unused
        # self.P = self.K @ self.Rt
        return

    @staticmethod
    def calc_K(f,pp_x,pp_y):
        K = np.array([
            [f, 0, pp_x],
            [0, f, pp_y],
            [0, 0, 1.0]
        ],dtype=np.float64)
        return K

    @staticmethod
    def calc_K_inv(f,pp_x,pp_y):
        K_inv = np.array([
            [1,0,-pp_x],
            [0,1,-pp_y],
            [0,0,f]
        ],dtype=np.float64) / f
        return K_inv

    @staticmethod
    def calc_rotation_translation_matrix(theta,delta):
        '''
        calculate rotation_translation matrix, where
        theta = [theta_x, theta_y, theta_z]' rotation angle (rad.) for each axis; numpy (3,1)
        delta = [dx, dy, dz]' translation (px) for each axis; numpy (3,1)

        :returns
        R: rotation matrix Rz@Ry@Rx
        t: translation vector (equal to delta)
        '''
        theta_x, theta_y, theta_z = theta

        s = np.sin
        c = np.cos

        Rx = np.array([
            [1., 0, 0],
            [0, c(theta_x), -s(theta_x)],
            [0, s(theta_x), c(theta_x)]
        ], dtype=np.float32)
        Ry = np.array([
            [c(theta_y), 0, s(theta_y)],
            [0, 1., 0],
            [-s(theta_y), 0, c(theta_y)]
        ], dtype=np.float32)
        Rz = np.array([
            [c(theta_z), -s(theta_z), 0],
            [s(theta_z), c(theta_z), 0],
            [0, 0, 1.]
        ], dtype=np.float32)
        R = Rz @ Ry @ Rx  # (3,3)

        t = delta
        return R, t


    @staticmethod
    def f_distort(xy,distortion_coef):
        '''
        distortion function using first 5 distortion model parameters
        https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
        '''
        x,y = xy
        k1,k2,p1,p2,k3 = distortion_coef

        r2 = x**2 + y**2
        xd = x*(1 + k1*r2 + k2*r2**2 + k3*r2**3) + 2*p1*x*y + p2*(r2 + 2*x**2)
        yd = y*(1 + k1*r2 + k2*r2**2 + k3*r2**3) + p1*(r2 + 2*y**2) + 2*p2*x*y

        xy_d = np.vstack((xd,yd))
        return xy_d


    @staticmethod
    def f_undistort(xy,undistortion_coef):
        # use same model, but different parameters
        return Camera.f_distort(xy,undistortion_coef)

    @staticmethod
    def world_to_image(XYZ1,K,theta,delta,distortion_coef,version,DEBUG_FLAG=False):
        '''
        convert from world point [X,Y,Z,1]' (homogeneous) to image point [x,y]' (non-homogeneous)
        using axes transform notation (camera is located at delta)

        XYZ1: (4,1)
        K: (3,3)
        theta: (3,1)
        delta: (3,1)
        distortion_coef: (5,)
        version: 'imatest' or 'opencv'

        distortion coefficients follow opencv equations
        https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
        version = 'imatest' or 'opencv'; use this to select imatest/opencv sequence of equations
        imatest: apply distortion last; extrinsic-->intrinsic-->distortion
        opencv: apply intrinsic matrix last; extrinsic-->distortion-->intrinsic
        '''
        R,t = Camera.calc_rotation_translation_matrix(theta,delta)

        RRt = np.hstack((R.T, -R.T@t))

        if version=='imatest':
            xyw = K @  RRt @ XYZ1
            xy = xyw[0:2] / xyw[2]
            xy_d = Camera.f_distort(xy,distortion_coef)
        elif version=='opencv': # hint: use this
            xyw = RRt @ XYZ1
            xy = xyw[0:2] / xyw[2]
            xyd = Camera.f_distort(xy,distortion_coef)
            xyd = np.vstack((xyd,[1.]))
            xy_d = K @ xyd
            if DEBUG_FLAG:
                print('xyw=', xyw.flatten(), 'xy=', xy.flatten(), 'xyd=', xyd.flatten(), 'RRt=', RRt.flatten())
        else:
            xy_d = np.nan
            print('invalid version; should be imatest or opencv')
        return xy_d[0:2] # (2,1); last element is 1.0

    @staticmethod
    def image_to_world_ray_with_distortion(xy,K_inv,theta,delta,undistortion_coef,depth): # TODO: change xy to xy1
        '''
        shoot ray: image coord. [x,y]'-> pinhole w/ distortion -> ray terminating at unit length in world coord. [X,Y,Z]'
        input units: px
        output units: px

        to project unit ray, set depth=1.0

        xy: (2,1)
        theta = [theta_x, theta_y, theta_z]' // rotation angle [rad.] for each axis
        delta = [dx, dy, dz]' // translation [px] for each axis
        XYZ: (3,1)

        using: axes transform notation, with opencv processing sequence (in reverse)
        '''

        # reminder: to calculate a batch of points, should use xy(N,2,1), theta(N,3,1), delta(N,3,1), ud_coef(N,5), depth(N,1)

        w = 1.0
        R, t = Camera.calc_rotation_translation_matrix(theta, delta)

        temp = K_inv @ np.vstack((xy,np.ones(xy[0].shape))) # K_inv @ xy1
        undistorted = Camera.f_undistort(temp[0:2],undistortion_coef) # undistorted xy
        undistorted = np.vstack((undistorted,np.ones(xy[0].shape))) # undistorted xy1

        XYZ = w * R @ undistorted
        XYZ_unit = XYZ / np.linalg.norm(XYZ,axis=0)
        XYZ = XYZ_unit * depth
        XYZ = XYZ + t
        return XYZ

class CameraSet:
    def __init__(self,N_cameras,camera_properties_dict_list):
        self.N_cameras = N_cameras
        self.camera_set = [] # list of camera instances
        for i in range(N_cameras):
            d = camera_properties_dict_list[i]
            self.camera_set.append(Camera(d))
        return

def build_KITTI_set():
    '''
    return KITTI_camera_properties_dict_list
    reminder: modify values when feeding these values into keras Input(resized image)
    values to modify: pp_x, pp_y, f
    '''
    # see KITTI_test.m and calib_cam_to_cam.txt for values
    # TODO: add "...\Google Drive\CS_AI_maths\computer vision\MATLAB\KITTI_test.m" to repo

    import copy
    N = 2 # use color cameras 02 and 03
    camera_properties_dict_list = []
    camera = GetCameraParameters('KITTI')
    camera_02_dict = copy.deepcopy(camera.get())  # get a copy of camera_properties_dict
    camera_03_dict = copy.deepcopy(camera.get())

    # set values for camera 02
    camera_02_dict['name'] = 'KITTI_cam_02'
    camera_02_dict['pp_x'] = 694.43
    camera_02_dict['pp_y'] = 236.34
    camera_02_dict['delta'] = np.array([[5.948968e-02, -8.603063e-04, 2.662728e-03]],dtype=np.float32).T # shape=(3,1)
    camera_02_dict['theta'] = np.array([[0.0047641,-0.0036199,0.0050537]],dtype=np.float32).T # shape=(3,1)
    camera_02_dict['f'] = 958.88

    # set values for camera 03
    camera_03_dict['name'] = 'KITTI_cam_03'
    camera_03_dict['pp_x'] = 694.62
    camera_03_dict['pp_y'] = 235.31
    camera_03_dict['delta'] = np.array([[-4.732167e-01, 5.830806e-03, -4.405247e-03]], dtype=np.float32).T
    camera_03_dict['theta'] = np.array([[0.0032194,-0.023496,-0.016668]], dtype=np.float32).T
    camera_03_dict['f'] = 903.25

    KITTI_camera_properties_dict_list = []
    KITTI_camera_properties_dict_list.append(camera_02_dict)
    KITTI_camera_properties_dict_list.append(camera_03_dict)

    return KITTI_camera_properties_dict_list

if __name__ == "__main__":
    camera = GetCameraParameters('g85 25mm')
    camera.info()

    # test CameraSet
    N = 3
    camera_properties_dict_list = []
    for i in range(N):
        camera_properties_dict_list.append(camera.get())
    cs = CameraSet(N,camera_properties_dict_list)
    for obj in cs.camera_set:
        print('---------------------------')
        obj.info()


    # test Camera
    c = Camera(camera.get())
    R,t = c.calc_rotation_translation_matrix(c.theta,c.delta)
    print(R,'\n',t)

    print('------- testing (un)distort functions ----------')
    xy = np.array([[1,2,3],[4,5,6.0]])/10
    c.undistortion_coef = np.array([0, 0, 0, 0, 0.])  # set all undistortion_coef to 0
    xd = c.f_distort(xy,c.distortion_coef)
    xud = c.f_undistort(xy, c.undistortion_coef)
    print(xud==xy) # should be true

    print('----------- testing world_to_image ------------')
    XYZ1 = np.array([[100.,100,1000,1]]).T # (4,1)
    theta = np.array([[1, 2, 3.]]).T  # (3,1)
    delta = np.array([[.1, .2, .3]]).T  # (3,1)
    xy = c.world_to_image(XYZ1, c.K, theta, delta, c.distortion_coef, 'opencv')
    print(xy.shape)


    print('----- testing image_to_world_ray_with_distortion() -------')
    xy = np.array([[1,2.]]).T # (2,1) # shoot 1 point
    theta =  np.array([[1,2,3.]]).T  #(3,1)
    delta = np.array([[.1,.2,.3]]).T # (3,1)

    XYZ = c.image_to_world_ray_with_distortion(xy,c.K_inv,theta,delta,c.undistortion_coef,1.0)
    print(XYZ.shape)

    xy = np.array([[1, 2.],[1,2.]]).T  # (2,Nbatch) # shoot 2 points; Nbatch=2; batch on last dimension
    theta = np.array([[1, 2, 3.],]).T  # (3,1)
    delta = np.array([[.1, .2, .3]]).T  # (3,1)
    depth = np.array([[1.0,2]]) # (1,Nbatch) # must be row vector ( np.array([[...]]) ) to broadcast properly!!

    XYZ = c.image_to_world_ray_with_distortion(xy, c.K_inv, theta, delta, c.undistortion_coef, depth)
    print(XYZ.shape)

    # test 2 different cameras
    print('----------- testing CameraSet: stereo camera ------------')
    N = 2 # stereo camera
    camera_properties_dict_list = []
    print(camera_properties_dict_list)
    camera = GetCameraParameters('g85 25mm')

    import copy
    camera_L_dict = copy.deepcopy(camera.get()) # get a copy of camera_properties_dict
    camera_R_dict = copy.deepcopy(camera.get())
    camera_L_dict['delta'] = np.array([[0,0,0]],dtype=np.float32).T # modify pose
    camera_L_dict['theta'] = np.array([[0,0,0]],dtype=np.float32).T
    camera_R_dict['delta'] = np.array([[1, 1, 1]], dtype=np.float32).T
    camera_R_dict['theta'] = np.array([[.1, .1, .1]], dtype=np.float32).T
    camera_L_dict['name'] = 'g85 25mm LEFT'
    camera_R_dict['name'] = 'g85 25mm RIGHT'

    camera_properties_dict_list.append(camera_L_dict)
    camera_properties_dict_list.append(camera_R_dict)

    cs = CameraSet(N, camera_properties_dict_list)
    for obj in cs.camera_set:
        print('---------------------------')
        obj.info()
    # DONE: environment.py -> environment.create_camera_set()


    # test build_KITTI_set()
    KITTI_camera_properties_dict_list = build_KITTI_set()
    cs_KITTI = CameraSet(2, KITTI_camera_properties_dict_list)
    for obj in cs_KITTI.camera_set:
        print('---------------------------')
        obj.info()


    print('-- done')