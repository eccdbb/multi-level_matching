This project:

stereo images -> optical flow -> point cloud -> point cloud flow (scene flow) in time

1. Run `flow.py`; enable sections using if False: -> if True:
2. save point cloud using x.save(), rename file
3. run `registration.py` to attempt point cloud matching via FPFH features

Conclusion so far:

Using SceneFlow() in `flow.py`, it is possible to naively do scene flow.
However, ORB (and similar) feature matching methods are naive.
On the other hand, optical flow favors local displacements (but the scene flow will be very difficult to visualize due to many lines).

Tested:

a) ORB scene flow using `SceneFlow.compute_flow_in_time_sparse()`

Could continue TODO test:

b) optical flow scene flow visualization using `SceneFlow.compute_flow_in_time()`

TODO:

c) geometry and color based scene flow

d) geometry, color, and kinematic model (velocity) based scene flow

CURRENTLY NEED:

- better depth estimation