import numpy as np

class Octree:
    '''
    given a point: 3-dimension vector (ex. RGB) with each elemtn in range 0..1,
    construct a octree histogram (each leaf of octree is a histogram bin)
    add points into octree and update histogram count

    see Octree.update

    use histogram as a feature vector for downstream tasks (ex. joint geometry color optimization/similarity)

    example:

    x = Octree()
    y = Octree()
    # add points to x and y
    point_list_1 = [[0.1,0.2,0.2], [0.333,0.3,0.3], ...]
    for point in point_list:
        f = x.get_feature(point, flatten=True)
        x.update(f)
    point_list_2 = ...
        f = y.get_feature(point, flatten=True)
        y.update(f)
    x.finalize()
    y.finalize()
    compute L2 norm: ||x.histogram - y.histogram||_2
    compute L0 norm: ||x.histogram_hamming - y.histogram_hamming||_0
    '''

    def __init__(self, N_levels=4):
        # call the midpoint x,y,z because color space could be RGB, HSV, Lab, ...
        self.mid_x = 0.5
        self.mid_y = 0.5
        self.mid_z = 0.5
        self.N_levels = N_levels # number of levels in octree; smaller -> more robust feature

        self.histogram = np.zeros(2**(self.N_levels*3), dtype=int) # feature vector for use with L1/L2 norm of difference of two vectors
        self.histogram_hamming = None # feature vector for use with hamming distance (# bit flips; L0 norm of difference of two vectors)
        return

    def get_feature(self, point, flatten=True, is_scalar=False):
        '''
        is_scalar = False -> 3D point
        is_scalar = True -> scalar point

        use this function to add points
        '''
        if not(is_scalar):
            X = point[0]
            Y = point[1]
            Z = point[2]

            temp_X = self.search(X) # list of length self.N_levels
            temp_Y = self.search(Y)
            temp_Z = self.search(Z)

            feature_vector = []
            feature_vector.append(np.asarray(temp_X).astype(np.int))
            feature_vector.append(np.asarray(temp_Y).astype(np.int))
            feature_vector.append(np.asarray(temp_Z).astype(np.int))

        else: # is scalar
            X = np.asarray(point)
            temp = self.search(X)
            feature_vector = []
            feature_vector.append(np.asarray(temp).astype(np.int))

        feature_vector = np.asarray(feature_vector)
        if flatten:
            out = feature_vector.flatten()
        else:
            out = feature_vector
        return out

    def search(self, point, DEBUG_FLAG=False):
        '''binary search given scalar in the interval [0,1]; clip if out of bounds'''

        if point > 1.0:
            point = 1.0
        if point < 0.0:
            point = 0.0

        count = 1
        number = []
        k = 0.5 # divide point
        ptr = 0.5
        while count <= self.N_levels:
            if DEBUG_FLAG: print(point, ptr, k)
            if point >= ptr:
                number.append('1')
                ptr += k/2
            else:
                number.append('0')
                ptr -= k/2
            k = k/2
            count += 1

        return number # list of length self.N_levels

    def update(self, feature):
        '''
        given a feature vector, increment the histogram count

        ex.: add one point
        x = Octree()
        point = [0.5, 0.8, 0.12345]
        f = x.get_feature(point, flatten=True)
        x.update(f)

        ex.: add many points from a list
        x = Octree()
        point_list = [[0.1,0.2,0.2], [0.333,0.3,0.3], ...]
        for point in point_list:
            f = x.get_feature(point, flatten=True)
            x.update(f)
        print(x.histogram)
        x.finalize(True) # normalize the histogram and compute histogram_hamming
        print(x.histogram)
        print(x.histogram_hamming)
        '''

        # each feature vector (ex. 0110) defines a bin
        # convert feature vector to index
        def bin2int(x):
            # https://stackoverflow.com/questions/46184684/converting-binary-numpy-array-into-unsigned-integer
            x = np.flip(x)  # read from LSB to MSB
            y = 0
            for i, j in enumerate(x):
                y += j << i
            return y

        idx = bin2int(feature)
        self.histogram[idx] += 1

        return

    def finalize(self, normalize=False):
        '''
        normalize histogram if True
        also, set self.histogram_hamming
        '''
        if normalize:
            total = self.histogram.sum()
            if total <= 0:
                print('warning: histogram is either empty or corrupted')
            self.histogram = self.histogram / total

            # since normalized, bin is 0..1
            # get this binary number array and use
            # hamming distance to compute similarity for downstream tasks

            # get binary number for each histogram value and compute the hamming feature vector
            temp = [self.get_feature(histbin,is_scalar=True) for histbin in self.histogram.flatten()] # lazy; reuse get_feature() which expects length 3 vector
            temp2 = np.stack((temp))
            temp2 = temp2[:,0:3]
            self.histogram_hamming = temp2.flatten()

        return

if __name__=="__main__":
    x=Octree(10)
    s = x.search(1/4+1/8+1/16) # 1/4+1/8+1/16  -->  0111000000...
    print(s)
    print(''.join(s))

    f1 = x.get_feature([1/2, 1/4, 1/8], flatten=False)
    print(f1)

    f2 = x.get_feature([1/2+1/8, 1/2+1/4+1/16+1/32, 1/2+1/4+1/8+1/16], flatten=True) # feature vector for xyz/RGB/HSV/Lab/...3-dimensional data
    print(f2)

    def bin2int(x):
        # https://stackoverflow.com/questions/46184684/converting-binary-numpy-array-into-unsigned-integer
        x = np.flip(x) # read from LSB to MSB
        y = 0
        for i,j in enumerate(x):
            y += j<<i
        return y

    y_in = [0,0,1,1]
    y = np.array(y_in)
    y2 = bin2int(y)
    print(y2) # 3

    print([bin2int(x) for x in f1])

    help(Octree.update)

    x2 = Octree(3)
    point_list = [[0.1, 0.2, 0.9], [0.333, 0.3, 0.3], [0.333, 0.3, 0.3], [0.333, 0.3, 0.3]]
    for point in point_list:
        f = x2.get_feature(point, flatten=True)
        x2.update(f)
    print(x2.histogram)
    x2.finalize(normalize=True)  # normalize the histogram
    print(x2.histogram)
    print(x2.histogram_hamming)
    #print(x2.histogram_hamming[x2.histogram_hamming>0])
    #for i in range(len(x2.histogram)):
    #    if np.any(x2.histogram[i]): print(x2.histogram[i])



    print('-- done')