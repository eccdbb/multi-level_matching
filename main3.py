# this file is the ancestor scratch file to flow.py

# from: https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html (https://github.com/opencv/opencv/blob/3.4/samples/python/tutorial_code/video/optical_flow/optical_flow_dense.py)
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

cap = cv.VideoCapture(cv.samples.findFile("vtest.avi"))
ret, frame1 = cap.read()
prvs = cv.cvtColor(frame1,cv.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[...,1] = 255

cv.imshow('frame', frame1)
count = 0
while(count < 5):
    ret, frame2 = cap.read()
    next = cv.cvtColor(frame2,cv.COLOR_BGR2GRAY)
    flow = cv.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    mag, ang = cv.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv.normalize(mag,None,0,255,cv.NORM_MINMAX)
    bgr = cv.cvtColor(hsv,cv.COLOR_HSV2BGR)
    cv.imshow('frame2',bgr)
    k = cv.waitKey(30) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv.imwrite('opticalfb.png',frame2)
        cv.imwrite('opticalhsv.png',bgr)
    prvs = next
    count += 1
print('-----------------')
# DONE: try using flow to estimate depth via inverse/back-projection; see flow.py

# panasonic g85
img_pair_list = []
img_pair_list.append({'left': 'P1510693.JPG', 'right': 'P1510692.JPG'}) # coin table
img_pair_list.append({'left': 'P1510695.JPG', 'right': 'P1510694.JPG'}) # room
img_pair_list.append({'left': 'P1510697.JPG', 'right': 'P1510696.JPG'}) # curtain, towel, chair
img_pair_list.append({'left': 'P1510699.JPG', 'right': 'P1510698.JPG'}) # curtain, socks on pole
img_pair_list.append({'left': 'P1510701.JPG', 'right': 'P1510700.JPG'}) # fabric aliasing
img_pair_list.append({'left': 'P1510703.JPG', 'right': 'P1510702.JPG'}) # phone glass reflection
pick = 2
resize_factor = 0.2
#img_L = cv.imread('P1510695.JPG')
#img_R = cv.imread('P1510694.JPG')
img_L = cv.imread(img_pair_list[pick]['left'])
img_R = cv.imread(img_pair_list[pick]['right'])
print(img_L.shape)
img_L = cv.cvtColor(img_L,cv.COLOR_BGR2GRAY)
img_R = cv.cvtColor(img_R,cv.COLOR_BGR2GRAY)
dim_L = tuple(reversed([int(x*resize_factor) for x in img_L.shape]))
dim_R = tuple(reversed([int(x*resize_factor) for x in img_R.shape]))
img_L = cv.resize(img_L,dim_L,interpolation=cv.INTER_LANCZOS4)
img_R = cv.resize(img_R,dim_R,interpolation=cv.INTER_LANCZOS4)
print(img_L.shape)


dims = list(img_L.shape)
dims.append(3)
LR = np.zeros(dims,dtype=np.uint8)

# reminder: BGR
LR[:,:,-1] = img_L # RED
LR[:,:,-3] = img_R # BLUE


pyr_scale = 0.5
levels = 7
winsize = 5 # averaging window size; larger values increase the algorithm robustness to image noise and give more chances for fast motion detection, but yield more blurred motion field
iter = 3
poly_n = 5 # size of the pixel neighborhood used to find polynomial expansion in each pixel; larger values mean that the image will be approximated with smoother surfaces, yielding more robust algorithm and more blurred motion field, typically poly_n =5 or 7.
poly_sigma = 1.2 # standard deviation of the Gaussian that is used to smooth derivatives used as a basis for the polynomial expansion
flags = cv.OPTFLOW_FARNEBACK_GAUSSIAN
flow_LR = cv.calcOpticalFlowFarneback(img_L, img_R, None, pyr_scale, levels, winsize, iter, poly_n, poly_sigma, flags)

# https://stackoverflow.com/questions/37871443/how-to-compute-optical-flow-using-tvl1-opencv-function
flow_LR_2 = cv.optflow.createOptFlow_DualTVL1()
flow_LR_2.create()
flow_LR_2.setScaleStep(0.5) # (689,918,3) ==> [x*0.5**7 for x in dim_L] ==> [7.171875, 5.3828125]
flow_LR_2.setScalesNumber(8)
flow_LR2_out = flow_LR_2.calc(img_L, img_R, None)

img_flow_LR = np.zeros(dims)
img_flow_LR[:,:,-1] = cv.normalize(flow_LR[:,:,0], 0, 255, cv.NORM_MINMAX) # RED
img_flow_LR[:,:,-3] = cv.normalize(flow_LR[:,:,1], 0, 255, cv.NORM_MINMAX) # BLUE
print(flow_LR.shape)

img_flow_LR2 = np.zeros(dims)
img_flow_LR2[:,:,-1] = cv.normalize(flow_LR2_out[:,:,0], 0, 255, cv.NORM_MINMAX) # RED
img_flow_LR2[:,:,-3] = cv.normalize(flow_LR2_out[:,:,1], 0, 255, cv.NORM_MINMAX) # BLUE
print(flow_LR2_out.shape)

_ = plt.hist(flow_LR[:,:,0].flatten(), bins='auto')
plt.title('x flow')
plt.show()

if True:
    #cv.imshow('img_L',img_L)
    #cv.imshow('img_R',img_R)
    cv.imshow('L_R', np.concatenate((img_L,img_R), axis=1))
    cv.imshow('LR', LR)
    #cv.imshow('flow_LR',img_flow_LR)
    #cv.imshow('flow_LR2',img_flow_LR2)
    cv.imshow('flow_LR_LR2', np.concatenate((img_flow_LR,img_flow_LR2), axis=1))
    cv.waitKey(1)

print('--done')

# : post-process: for x and y flow: for pixels with 0 flow, use k-nearest-neighbors (non-zero values) to fill in
# DONE: take pictures with less flow (smaller L to R distance)

# TODO: calculate forwards-backward endpoint error // see "Estimation and Analysis of Motion in Video Data - see forwards backwards endpoint error.pdf"
# for optical flow function f_LR (forwards flow) and f_RL (backwards flow), error = ||f_RL( x + f_LR(x) )||

# also see:
# https://developer.nvidia.com/blog/opencv-optical-flow-algorithms-with-nvidia-turing-gpus/

# TODO: make new project: optical flow to IMU velocity estimation (supervised); use KITTI synced+rectified
# flatten optical flow image into vector -> Dense -> Dense -> out